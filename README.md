# VigiData Analytics

The static-website is available here: https://manjalyc.gitlab.io/vigidata-analytics/

# Development Notice

This project is under heavy active development and is in alpha. As such its usage is not recommended.

# License

This code is licensed under the open-source **BSD 3-clause "New" or "Revised" License**.

Please consult the attached [LICENSE](https://gitlab.com/manjalyc/vigidata-analytics/-/blob/master/LICENSE) file for details. All rights not explicitly granted by the BSD 3-clause "New" or "Revised" License are reserved by the Original Author.


## Attributions
- **Babel v7.10.3**
   * https://babeljs.io/
   * Transcompiling JS for a consistent cross-platform/browser experience.
   * Open-source [MIT License](https://github.com/babel/babel/blob/main/LICENSE)
- **Bootstrap v4.5.0**
   * https://getbootstrap.com/
   * Responsive Website UI Design
   * Open-source [MIT License](https://github.com/twbs/bootstrap/blob/main/LICENSE)
- **Chart.js v2.9.3**
	* https://www.chartjs.org/
	* Plot Generation
	* Open-source [MIT License](https://github.com/chartjs/Chart.js/blob/master/LICENSE.md)
- **Popper v1.16.1**
  * https://popper.js.org/
  * Enhances UI experience
  * Open-source [MIT License](https://github.com/popperjs/popper-core/blob/master/LICENSE.md)
- **SheetJs Community Edition v0.16.2**
	* https://sheetjs.com/
	* Loading Excel Files in-browser
	* Open-source [Apache License 2.0](https://github.com/SheetJS/sheetjs/blob/master/LICENSE)
-  Special Thanks to **Gitlab** for free static hosting
	* https://gitlab.com/
