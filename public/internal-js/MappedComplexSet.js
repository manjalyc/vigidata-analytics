'use strict';
//const ComplexSet = require('./ComplexSet.js');

//Map, key: any standard key, value: a ComplexSet
//if a key isn't in the set, the value of that key should correspond to the complement of the empty set
class MappedComplexSet extends Map {
	static getCopy(MappedComplexSetA){
		return MappedComplexSetA.getCopy();
	}

	static union(MappedComplexSetA, MappedComplexSetB){
		let ret = MappedComplexSetA.getCopy();
		ret.unionUpdate(MappedComplexSetB)
		return ret;
	}

	static intersection(MappedComplexSetA, MappedComplexSetB){
		let ret = MappedComplexSetA.getCopy();
		ret.intersectionUpdate(MappedComplexSetB);
		return ret;
	}

	static difference(MappedComplexSetA, MappedComplexSetB){
		let ret = MappedComplexSetA.getCopy();
		ret.differenceUpdate(MappedComplexSetB)
		return ret;
	}

	static symmetricDifference(MappedComplexSetA, MappedComplexSetB){
		let ret = MappedComplexSetA.getCopy();
		ret.symmetricDifferenceUpdate(MappedComplexSetB)
		return ret;
	}

	getCopy(){
		let copy = new MappedComplexSet();
		for(let [key, ComplexSetB] of this.entries()){
			copy.set(key, new ComplexSet(ComplexSetB));
		}
		return copy
	}

	unionUpdate(MappedComplexSetB){
		for(let [key, ComplexSetB] of MappedComplexSetB.entries()){
			//console.log(key, this.get(key), ComplexSetB)
			if(this.has(key)) this.get(key).unionUpdate(ComplexSetB);
		}
	}

	intersectionUpdate(MappedComplexSetB){
		for(let key of this.keys()){
			//console.log(key, '1', this.get(key).toStr())
			if(!MappedComplexSetB.has(key)) this.delete(key);
			else{
				//console.log(key, '2', MappedComplexSetB.get(key).toStr())
				this.get(key).intersectionUpdate(MappedComplexSetB.get(key));
			}
			//console.log(key, '3', this.get(key).toStr())
			//console.log();
		}
		for(let [key, ComplexSetB] of MappedComplexSetB.entries()){
			if(!this.has(key)) this.set(key, ComplexSetB.getCopy())
		}
		/*
		for(let [key, ComplexSetB] of MappedComplexSetB.entries()){
			if(!this.has(key)) this.delete(key);
			else this.get(key).intersectionUpdate(ComplexSetB);
		}
		*/
	}

	differenceUpdate(MappedComplexSetB){
		for(let [key, ComplexSetB] of MappedComplexSetB.entries()){
			if(this.has(key)){
				this.get(key).differenceUpdate(ComplexSetB);
			}
			else this.set(key, ComplexSet.complement(ComplexSetB))
		}
	}

	symmetricDifferenceUpdate(MappedComplexSetB){
		for(let [key, ComplexSetB] of MappedComplexSetB.entries()){
			if(this.has(key)){
				this.get(key).symmetricDifferenceUpdate(ComplexSetB);
			}
			else this.set(key, ComplexSet.complement(ComplexSetB))
		}
	}

	toStr(){
		let ret = `MappedComplexSet(${this.size}) {\n`
		for(let [key, aComplexSet] of this.entries()){
			ret += `\t(${key}): ${aComplexSet.toStr()}\n`
		}
		ret += "}"
		return ret
	}
}

//module.exports = MappedComplexSet

/*Proper Output of testMappedComplexSet()
Union
MappedComplexSet(3) [Map] {
  'A' => ComplexSet(4) [Set] { 1, 2, 3, 4, complementMode: false },
  'B' => ComplexSet(5) [Set] { 2, 3, 4, 5, 6, complementMode: false },
  'C' => ComplexSet(6) [Set] { 3, 4, 5, 6, 1, 2, complementMode: false }
}
Intersection
MappedComplexSet(3) [Map] {
  'A' => ComplexSet(2) [Set] { 2, 3, complementMode: false },
  'B' => ComplexSet(1) [Set] { 3, complementMode: false },
  'C' => ComplexSet(2) [Set] { 3, 4, complementMode: false }
}
Difference
MappedComplexSet(3) [Map] {
  'A' => ComplexSet(2) [Set] { 1, 4, complementMode: false },
  'B' => ComplexSet(1) [Set] { 2, complementMode: false },
  'C' => ComplexSet(2) [Set] { 5, 6, complementMode: false }
}
Symmetric Difference
MappedComplexSet(3) [Map] {
  'A' => ComplexSet(2) [Set] { 1, 4, complementMode: false },
  'B' => ComplexSet(4) [Set] { 2, 4, 5, 6, complementMode: false },
  'C' => ComplexSet(4) [Set] { 5, 6, 1, 2, complementMode: false }
}
*/
//testMappedComplexSet()
function testMappedComplexSet(){
	let myMap1 = new MappedComplexSet()
	let myMap2 = new MappedComplexSet()

	let reInit = function(){
		myMap1 = new MappedComplexSet()
		myMap1.set('A', new ComplexSet([1, 2, 3, 4]))
		myMap1.set('B', new ComplexSet([2, 3]))
		myMap1.set('C', new ComplexSet([3, 4, 5, 6]))

		myMap2 = new MappedComplexSet()
		myMap2.set('A', new ComplexSet([2, 3]))
		myMap2.set('B', new ComplexSet([3, 4, 5, 6]))
		myMap2.set('C', new ComplexSet([1, 2, 3, 4]))
	}

	reInit()
	//console.log('MappedComplexSet A')
	//console.log(myMap1)
	//console.log('MappedComplexSet B')
	//console.log(myMap2)
	console.log('Union')
	console.log(MappedComplexSet.union(myMap1, myMap2));
	console.log('Intersection')
	console.log(MappedComplexSet.intersection(myMap1, myMap2));
	console.log('Difference')
	console.log(MappedComplexSet.difference(myMap1, myMap2));
	console.log('Symmetric Difference')
	console.log(MappedComplexSet.symmetricDifference(myMap1, myMap2));
}
