//let eD;

function removeComment(line){
  commentStartLoc=line.indexOf('//')
  if (commentStartLoc === -1) return line;
  return line.substring(0,commentStartLoc).trim();
}

function getFilter(input, filters) {
  let AFL = AdvancedFilterLogic;
  if(typeof(input) === 'object') return AFL.normalizeIDs(eD.getBasicUniversalFilter(...input), eD.cases, eD.IDcolumn);;

  input = input.replace(/,/g,' , ')
  input = input.replace(/\(/g,'( ')
  input = input.replace(/\)/g,' )')
  for(let property in filters){
    let re = new RegExp(` ${property} `, "g")
    input = input.replace(re,`filters['${property}']`)
  }


  input = input.replace(/A\(/g,'AFL.complement(')

  input = input.replace(/_U\(/g,'AFL._union(')
  input = input.replace(/_I\(/g,'AFL._intersection(')
  input = input.replace(/_D\(/g,'AFL._difference(')
  input = input.replace(/_S\(/g,'AFL._symmetricDifference(')

  input = input.replace(/U\(/g,'AFL.union(')
  input = input.replace(/I\(/g,'AFL.intersection(')
  input = input.replace(/D\(/g,'AFL.difference(')
  input = input.replace(/S\(/g,'AFL.symmetricDifference(')
  //console.log(input)

  //let Universal = TODO 
  return AFL.normalizeIDs(eval(input), eD.cases, eD.IDcolumn);
  //
}

//Input Processing
function matchClosingBracket(str, startIndex){
  //console.log(str.charAt(startIndex));
  count = 1;
  idx = startIndex;
  while( count > 0 ){
    idx++;
    if(str.charAt(idx) === '{') count++;
    else if(str.charAt(idx) === '}') count--;
  }
  //console.log(str.charAt(idx), idx-startIndex);
  return idx
}

function getFilters(input){
  aFi = input.indexOf('AvailableFilters');
  if (aFi === -1) {
    handleError(new Error('AvailableFilters not found'));
    console.error('Will attempt to continue with no filters pre-processed')
    return null;
  }
  let filters = input.substring(aFi);
  filters = input.substring(input.indexOf('{'));
  filters = filters.substring(0,matchClosingBracket(filters,0)+1)
  filters = filters.replace(/\n/g,' ');
  let pfilters;
  try{
    pfilters = JSON.parse(filters)
  }
  catch(err){
    console.error('AvailableFilters is not in proper JSON format, the following failed to parse:');
    console.error(filters);
    console.error('Refer to https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/JSON_bad_parse');
    handleError(err);
    return null
  }

  filters = {}
  for(let property in pfilters){
    //console.log(property);
    filters[property] = getFilter(pfilters[property], filters)
    nID = filters[property][0].size;
    nID_m = filters[property][0].complementMode;
    nSequenced = filters[property][1].size;
    console.log("[INFO]", `Processed Filter ${property}: Total IDs ${nID} (${nID_m}), Total Specially Sequenced IDs ${nSequenced}`)
    //console.log(filters[property])
  }
  return filters
}

function processCommand(filters, command){
  let originalCommand = command;
  if (command.indexOf("'") !== -1){
    console.error(`[README] Warning the command: '${command}' contains single quotes (') instead of double quotes ("), attempting to continue`)
  }
  console.log("[INFO] Processing command:", command)
  let currFilter = undefined;
  let filterName = undefined;
  if (command.indexOf(':') > 0 && command.indexOf(':') < command.indexOf(' ')){
    currFilter = filters[command.split(':')[0]]
    filterName = command.split(':')[0];
    command = command.substring(command.indexOf(':')+1)
  }
  command = command.trim()
  command = 'eD.' + command.replace(' ','(')
  if(command.indexOf('(') === -1) command += '('
  command += ')'
  command = command.replace(/\" \"/g,'", "')

  //console.log(command);
  if(currFilter) eD.setFilter(currFilter);
  let n = eD.getN();
  if ( command.indexOf('eD.get') === 0 ) ret = eval(command);
  else if(command.indexOf('eD.plot') === 0) {
    command = 'PlotCreator'+command.substring(2);
    if(filterName) command = command.slice(0,-1) + `, filterName = "${filterName}")`
    //console.log(command);
    eval(command)
    ret = 'See Plots'
  }
  else{
    console.error(`[README] Failed to recognize command: '${originalCommand}', it was transcompiled to ${command}`)
    ret = `ERROR: ${command}`
  }
  if(currFilter) eD.clearFilters();
  return [command, filterName, n, ret];
}

function processCommands(filters, input, prettyPrint = false){
  let aFI = input.indexOf('AvailableFilters');
  if (aFI !== -1 ){
    input = input.substring(aFI);
    input = input.substring(input.indexOf('{'));
    input = input.substring(matchClosingBracket(input,0)+1).trim();
  }
  let output = ""

  pM.clearPlots();
  for(let command of input.split('\n')){
    if(command.trim() !== ''){
      let [commandline, currFilter, n, ret] = processCommand(filters, command)
      output += `Commandline: ${commandline}\n`
      if(currFilter) output += `  Filter Name: ${currFilter}\n`
      output += `  # of Processed IDs: ${n}\n`
      if(prettyPrint) output += `  JSON Command Output: ${JSON.stringify(ret, null, '    ')}\n\n`;
      else output += ` JSON Command Output: ${JSON.stringify(ret)}\n\n`;
    }
  }
  pM.refreshPlots();
  return output
}

function removeComments(raw_input){
  let input = ''
  //strip comments
  for(let i = 0; i < raw_input.length; i++){
    line = removeComment(raw_input[i]);
    if (line !== ''){
      input += line
      input += '\n'
      //console.log(line);
    }
  }
  return input
}

function processInput(prettyPrint = false, timeoutForModal=true){
  if(timeoutForModal){
    showProcessingModal()
    //console.log('b', timeoutForModal);
    setTimeout( function(){processInput(prettyPrint = prettyPrint, timeoutForModal=false)},500);
    return
  }

  try {
    let raw_input = document.getElementById('inputOnPage').value.split('\n');
    let input = removeComments(raw_input)

    //get filters
    let lT
    lT = 0 - performance.now();
    filters = getFilters(input);
    lT += performance.now();
    console.log('[INFO][PERF] Filters precalculated in', lT/1000, 's')

    //run commands
    lT = 0 - performance.now();
    output = processCommands(filters, input, prettyPrint = prettyPrint)
    lT += performance.now();
    document.getElementById('outputOnPage').value = output
    console.log('[INFO][PERF] Commands processed in', lT/1000, 's')
    hideProcessingModal();
    //create plots
  }
  catch(err){
    handleError(err);
      let pMF = document.getElementById('processingModal_footer')
      pMF.innerHTML = '<p><b>A Critical unexpected exception occured. Please press force close and check the Progress Log.</b></p><button type="button" data-dismiss="modal" data-target="#processingModal" class="btn btn-warning">Force Close</button>'
  }
}
