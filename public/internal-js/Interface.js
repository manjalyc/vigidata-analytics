var eD = null;
let eD_configuration = {
  "Sheets": ["Cases","Drugs","Reactions"],
  "UniqueIDidentifer": "UMC report ID",
  "generateCustomDataPoints": true
}
/**Another Old Configuration
{
  "Sheets": [
    "CaseInfo"
  ],
  "UniqueIDidentifer": "ReportID",
  "generateCustomDataPoints": false
}
**/

//edit Configuration
function editConfiguration(){
  document.getElementById('editConfigModal_textarea').value = JSON.stringify(eD_configuration, null, '  ');
  $('#editConfigModal').modal('show')
}

function saveConfiguration(){
  try{
  eD_configuration = JSON.parse(document.getElementById('editConfigModal_textarea').value);
  }
  catch(err) { handleError(err)}
  console.log('[INFO] Configuration Updated: ', eD_configuration)
  $('#editConfigModal').modal('show')

}



//Loading File
let LoadFile = function LoadFile(opts){
  let undefinedFunc = function(){};

  function handleFile(event){
    event.stopPropagation();
    event.preventDefault();
    let file = event.target.files[0]

    //console.log('Available file properties:')
    //for (let property in file) console.log('- ',property);

    function vigiLoadWorkbook(event){
      //showLoadingFileModal();
      if (!event || !event.target || !event.target.files || event.target.files.length === 0) return;
      const filename = event.target.files[0].name;
      //document.getElementById('vigiLoadWorkbook_label').textArea
      document.getElementById('vigiLoadWorkbook_label').innerHTML = filename
      //readSingleFile(event);
      try{
        readExcelFile(event);
      }
      catch(err){
        handleError(err)
      }
    }

    if (file.size > 2e5) {
      let estimatedMaxLoadTime = Math.ceil(file.size/4e5)
      let message = `${file.name} is a large file (${file.size} Bytes)`
      message += '\nThis page may freeze while the file is being read into memory.'
      message += '\nEstimated Max Load Time: ' + estimatedMaxLoadTime + ' seconds.'
      message += '\nEstimated Max Processing Time: ' + Math.ceil(estimatedMaxLoadTime/10) + ' second(s).'
      message += '\nPress OK to start.'
      showLoadingFileModal(file.name);
      alert(message);
      if(opts.preload) opts.preload;
    }
    setTimeout( function() {vigiLoadWorkbook(event); } , 1);
  }

  //reads into eD
  function readExcelFile(event){
    let file = event.target.files[0];
    if(!file) return;

    let reader = new FileReader();
    reader.onloadstart = function(anEvent){
    }
    reader.onload = function(anEvent){
      try{
        let loadFileTime;
        loadFileTime = 0 - performance.now();
        let data = new Uint8Array(anEvent.target.result);
        let workbook = XLSX.read(data, {type: 'array'});
        data = null;
        loadFileTime += performance.now()
        console.log('[INFO][PERF] Workbook Loaded in', loadFileTime/1000, 's')

        loadFileTime = 0 - performance.now();
        eD = new VigiWrapper(workbook, eD_configuration);
        workbook = null;
        loadFileTime += performance.now();
        console.log('[INFO][PERF] Processed Data in', loadFileTime/1000, 's')

        //Enable Input Buttons
        $('#notifyLoadSuccess').toast({delay: 10000});
        $('#notifyLoadSuccess').toast('show');
        enableInputButtons();

        //Report Data Integrity
    		if( !eD.dataIntegrity){
          //onsole.log('asdf', eD.dataIntegrity, eD.lastGeneratedDP);
          throw new Error('Some IDs failed to pass data integrity checks and were deleted (see above).  This is a nonfatal error, all other IDs passed initial validation. Input buttons have been enabled, you may continue.');
        }

        //Update Loading Modal, and attempt to hide it
        let lMF = document.getElementById('loadingFileModal_footer')
        lMF.innerHTML = '<p>Loading successfully completed, but this dialog failed to close. Please press force close to continue.</p><button type="button" data-dismiss="modal" data-target="#loadingFileModal" class="btn btn-success">Force Close</button>'
        setTimeout( function() {hideLoadingFileModal();} , 1);
        //lMF.innerHTML += 'Loading complete, this dialog failed to close, press force close to continue.'
      }
      catch(err){
        let lMF = document.getElementById('loadingFileModal_footer')
        lMF.innerHTML = '<p>An Exception occured. Please press force close and check the Progress Log.</p><button type="button" data-dismiss="modal" data-target="#loadingFileModal" class="btn btn-warning">Force Close</button>'
        disableLoadingFileModalSpinner();
        handleError(err)
      }
    };

    reader.readAsArrayBuffer(file)
  }

  if(opts.inputElement){opts.inputElement.addEventListener('change', handleFile, false)}
  if(opts.dropElement){opts.dropElement.addEventListener('drop', handleFile, false)}
}

//NON Specific Element Events
function enableInputButtons(){
  document.getElementById('processCommandsBtn').removeAttribute("disabled");
  document.getElementById('processCommandsBtn_pretty').removeAttribute("disabled");
  //document.getElementById('generateOverviewPlots').removeAttribute("disabled");
  document.getElementById('smallScreenPlotSwitch').removeAttribute("disabled");
  document.getElementById('createCommandHelper_btn').removeAttribute("disabled");
  document.getElementById('createFilterHelper_btn').removeAttribute("disabled");
}

function copyToClipboard(ID, isTextArea = false) {
  let element = document.getElementById(ID);
  let toCopy;
  if (isTextArea) toCopy = element.value;
  else toCopy = element.innerHTML;
  toCopy = toCopy.replace(/<\/span>/g,'');
  toCopy = toCopy.replace(/<br>/g,'\n');
  toCopy = toCopy.replace(/<br \/>/g,'\n');
  toCopy = toCopy.replace(/<span class="bg-danger">/g,'');
  navigator.clipboard.writeText(toCopy)
  //console.log("Copied", ID)
}

//Plot Stuff
function plotSwitchUpdate(){
  if (document.getElementById('smallScreenPlotSwitch').checked){
    pM.setMaxPerRow(1);
  }
  else{
    pM.setMaxPerRow(2)
  };
}


//Processing Modal
function enableProcessingModalSpinner(){
  document.getElementById('processingModal_spinner').innerHTML = '<div class="spinner-grow text-primary" ></div>';
}
function disableProcessingModalSpinner(){
  document.getElementById('processingModal_spinner').innerHTML = '';
}
function showProcessingModal(){
  let mTitle = document.getElementById('processingModal_title')
  mTitle.innerHTML = 'Processing Filters & Commands'
  enableProcessingModalSpinner();
  document.getElementById('processingModal_footer').innerHTML = '<button type="button" data-dismiss="modal" data-target="#processingModal" class="btn btn-danger">Force Close</button>'
  $('#processingModal').modal('show')
}

function hideProcessingModal(){
  disableProcessingModalSpinner();
  document.getElementById('processingModal_footer').innerHTML = '<b><p>Processing successfully completed, but this dialog failed to close. Please press force close to continue.</p></b><button type="button" data-dismiss="modal" data-target="#processingModal" class="btn btn-success">Force Close</button>'
  $('#processingModal').modal('hide');
}


//Loading Modal
function enableLoadingFileModalSpinner(){
  document.getElementById('loadingFileModal_spinner').innerHTML = '<div class="spinner-grow text-primary" ></div>';
}
function disableLoadingFileModalSpinner(){
  document.getElementById('loadingFileModal_spinner').innerHTML = '';
}
function showLoadingFileModal(filename){
  let mTitle = document.getElementById('loadingFileModal_title')
  mTitle.innerHTML = 'Loading Large File'
  if(filename) mTitle.innerHTML += `: ${filename}`
  enableLoadingFileModalSpinner();
  document.getElementById('loadingFileModal_footer').innerHTML = '<button type="button" data-dismiss="modal" data-target="#loadingFileModal" class="btn btn-danger">Force Close</button>'
  $('#loadingFileModal').modal('show')
}
function hideLoadingFileModal(){
  disableLoadingFileModalSpinner();
  $('#loadingFileModal').modal('hide')
}



//Input/Output Specific-Stuff
function deleteElement(elementID){
  let e = document.getElementById(elementID);
  e.parentNode.removeChild(e);
}

function splitIOSwitchUpdate(){
  if (document.getElementById('smallScreenIOSwitch').checked){
    disableSplitIO();
    document.getElementById('smallScreenIOSwitch').checked = true;
  }
  else{
    enableSplitIO();
  };
}
function clearTextArea(ID){
  document.getElementById(ID).value = '';
}

let inputTemplates = {
  "Overview": '//Template: Overview\n//The following template provides an overview of the data\n//Consult the "Filter Showcase" template to see the simplicity & power of filters"\nAvailableFilters = {\n  "suspect_drugs" : [ "Drugs", "Role", "Suspect" ]\n}\n\n//Commands to generate output\ngetDataTerms "Cases" "Country of primary source"\ngetDataTerms "Cases" "Custom|Continent of primary source"\ngetDataTerms "Cases" "Sex"\ngetNumMatches "Cases" "Country of primary source" "United States of America"\ngetOneVarStats "Cases" "Age"\n\n//Commands to generate Plots\nplotPieChart "Cases" "Country of primary source"\nplotBarChart "Cases" "Custom|Continent of primary source"\nplotDoughnotChart "Cases" "Sex"\nplotPieChart "Drugs" "WHODrug active ingredient"\nsuspect_drugs: plotPieChart "Drugs" "WHODrug active ingredient"\n\ngetDataPoints //dumps all usable datapoints at the end of the output',

  "Filter Showcase": '//Template: Filter Showcase\n//The following template showcases how to create and apply basic & advanced filters\n\nAvailableFilters = {\n  //Basic Filters\n  //filter_name: ["Sheet", "Column", "Search Term"]\n  "males" : [ "Cases" , "Sex", "Male" ],\n  "american_patients" : [ "Cases", "Country of primary source", "United States of America" ],\n\n  //Advanced Filters\n  //TODO: Enable Universal Set\n  //I = Intersection, U = Union, D = Difference, S = Symmetric Difference\n  "american_males" : "I(males, american_patients)",\n  "non_american_males" : "D(males, american_patients)",\n}\n\n\n//Commands to generate output\ngetDataTerms "Cases" "Country of primary source"\namerican_patients: getDataTerms "Cases" "Country of primary source"\n\ngetDataTerms "Cases" "Custom|Continent of primary source"\nnon_american_males: getDataTerms "Cases" "Custom|Continent of primary source"\n\ngetDataTerms "Cases" "Sex"\namerican_males: getDataTerms "Cases" "Sex"\n\ngetNumMatches "Cases" "Country of primary source" "United States of America"\nmales: getNumMatches "Cases" "Country of primary source" "United States of America"\n\ngetOneVarStats "Cases" "Age"\nmales: getOneVarStats "Cases" "Age"\n\n//Commands to generate Plots\nplotPieChart "Cases" "Country of primary source"\nmales: plotPieChart "Cases" "Country of primary source"\namerican_patients: plotPieChart "Cases" "Country of primary source"\namerican_males: plotPieChart "Cases" "Country of primary source"\nnon_american_males: plotPieChart "Cases" "Country of primary source"\n\nplotBarChart "Cases" "Custom|Continent of primary source"\nmales: plotBarChart "Cases" "Custom|Continent of primary source"\namerican_patients: plotBarChart "Cases" "Custom|Continent of primary source"\namerican_males: plotBarChart "Cases" "Custom|Continent of primary source"\nnon_american_males: plotBarChart "Cases" "Custom|Continent of primary source"\n\nplotDoughnotChart "Cases" "Sex"\nmales: plotDoughnotChart "Cases" "Sex"\namerican_patients: plotDoughnotChart "Cases" "Sex"\namerican_males: plotDoughnotChart "Cases" "Sex"\nnon_american_males: plotDoughnotChart "Cases" "Sex"\ngetDataPoints //dumps all usable datapoints at the end of the output',

  "Clean Start": 'AvailableFilters = {}',
}

function initInputTemplate(){
  setTemplate("Overview");
  let iTD = document.getElementById('templateDropDown');
  iTD.innerHTML = ""
  for(let property in inputTemplates){
    iTD.innerHTML += `<div class="dropdown-item" id="templateDropdownElement" onclick="setTemplate('${property}')">${property}</div>`
  }
}
function setTemplate(propertyName){
  document.getElementById('useTemplateButton').innerHTML = `Template: ${propertyName}`;
  document.getElementById('inputOnPage').value = inputTemplates[propertyName]
}


//Console/Progress Log Stuff
function consoleOnPage_clear() {
  let pageConsole = document.getElementById('consoleOnPage');
  pageConsole.innerHTML = '';
  initPageConsole();
}
function initPageConsole(){
  let pageConsole = document.getElementById('consoleOnPage');
  pageConsole.innerHTML = '';
  console.log("Initiated:", new Date())
  console.log("User-Agent:", navigator.userAgent)
  console.error("Caught exceptions will be highlighted");
}

//Errorhandling
function handleError(err){
  console.error(err.name, "Caught");
  console.error("Error Message:", err.message);
  console.error("Stack Trace")
  console.error(err['stack']);
}


//Create Basic Filter (createFilterHelperModal)
function addtoAvailableFilters(){
  let input = document.getElementById('inputOnPage').value;
  let cB = input.indexOf('AvailableFilters');
  if (cB === -1) {
    handleError(new Error('AvailableFilters not found, could not add filter'));
    console.error('Please ensure the input has near the top: AvailableFilters = {}')
  }
  cB = input.indexOf("{", cB);
  cB = matchClosingBracket(input, cB)-1;
  while(input.charAt(cB) === ' ' || input.charAt(cB) === '\n') cB--;
  cB += 1;
  let newInput = input.substring(0,cB);
  if(input.charAt(cB-1) !== '{') newInput += ',';
  newInput += `\n  ${createFilterHelper_filter}`;
  if(input.charAt(cB-1) === '{') newInput += '\n';
  newInput += input.substring(cB);
  document.getElementById('inputOnPage').value = newInput;
  //console.log('Critical TODO: 1')
}

let createFilterHelper_sheet = undefined;
let createFilterHelper_column = undefined;
let createFilterHelper_searchTerm = undefined;
let createFilterHelper_name = undefined;
let createFilterHelper_filter = undefined;

function createFilterHelper(stage = 0){
  let mB = document.getElementById('createFilterHelperModal_body');
  let nMB = "ERROR?";
  function getButton(attributes, value){
    let btnType = "btn-outline-success"
    if( value.indexOf('Custom') === 0) btnType = "btn-outline-dark"
    return `<button type="button" data-dismiss="modal" data-target="#createFilterHelperModal" class="btn ${btnType} buttonpadding" ${attributes}'>${value}</button>`
  }
  if(stage === 0){
    let nMBf = document.getElementById('createFilterHelperModal_footer');
    nMBf.innerHTML = '<button type="button" data-dismiss="modal" data-target="#createFilterHelperModal" class="btn btn-danger">Cancel</button>'
    nMB = "<h5>Please Select an Available Sheet</h5>"
    for(let sheetName in eD.iterableKeys){
      nMB += getButton(`onclick='createFilterHelper_sheet="${sheetName}"; createFilterHelper(stage = 1)'`, sheetName);
    }
  }
  else if(stage === 1){
    //console.log(eD.iterableKeys[createFilterHelper_sheet])
    nMB = "<h5>Please Select an Available Column (Datapoint)</h5>"
    for(let colName in eD.iterableKeys[createFilterHelper_sheet]){
      nMB += getButton(`onclick='createFilterHelper_column="${colName}"; createFilterHelper(stage = 2)'`, colName);
      //console.log(colName);
    }
  }
  else if(stage === 2){
    nMB = "<h5>Enter your search term (dataterm)</h5>";
    nMB += '<div class="input-group mb-3">'
      nMB += '<input type="text" class="form-control" placeholder="type your term here" id="createFilterHelper_inputTerm"><div class="input-group-append">'
      nMB += `<button onclick='createFilterHelper_searchTerm= document.getElementById("createFilterHelper_inputTerm").value;createFilterHelper(stage = 3)' class="btn btn-success" type="submit">Submit</button>`;
      nMB += '</div>';
    nMB += '</div>';

    nMB += "<h5>...or select from these found dataterms</h5>";
    let dT = eD.getDataTerms(createFilterHelper_sheet, createFilterHelper_column)
    for(property in dT){
      nMB += getButton(`onclick='createFilterHelper_searchTerm="${property}"; createFilterHelper(stage = 3)'`, `${property} (${dT[property]})`);
      //console.log(colName);
    }
  }
  else if(stage === 3){
    nMB = "<h5>Almost Done: Give your filter a name</h5>";
    nMB += '<div class="input-group mb-3">'
      nMB += '<input type="text" class="form-control" placeholder="type a name for your filter here" id="createFilterHelper_inputTerm"><div class="input-group-append">'
      nMB += `<button onclick='createFilterHelper_name= document.getElementById("createFilterHelper_inputTerm").value;createFilterHelper(stage = 4)' class="btn btn-success" type="submit">View Filter</button>`;
      nMB += '</div>';
    nMB += '</div>';
  }
  else if(stage === 4){
    createFilterHelper_filter = `"${createFilterHelper_name}": [ "${createFilterHelper_sheet}", "${createFilterHelper_column}", "${createFilterHelper_searchTerm}"]`
    nMB = "<h5>Your Filter</h5>"
    nMB += `<p class="text-center bg-dark text-light rounded" style="font-family: 'Courier New'">${createFilterHelper_filter}</p>`

    nMB += `<button onclick='navigator.clipboard.writeText(createFilterHelper_filter)' class="btn btn-primary text-right">Copy to Clipboard</button>`

    let nF = "";
    nF += `<button type="button" data-dismiss="modal" data-target="#createFilterHelperModal" class="btn btn-danger">Close (Don\'t Add Filter)</button>`
    nF += `<button type="button" data-dismiss="modal" data-target="#createFilterHelperModal" class="btn btn-success" onclick='addtoAvailableFilters()'>Add Filter and Close</button>`
    document.getElementById('createFilterHelperModal_footer').innerHTML = nF;
  }
  mB.innerHTML = nMB;
  $('#createFilterHelperModal').modal('show');
}


//Create Command
let createCommandHelper_dict = {
  "plotPieChart": {
    description: "Display a pie-chart with a count of unique IDs under each term in a column.",
    steps: ["createCommandHelper_sheet()", "createCommandHelper_column()","createCommandHelper_finalize()"]
  },
  "plotDoughnotChart": {
    description: "Display a doughnot-chart with a count of unique IDs under each term in a column.",
    steps: ["createCommandHelper_sheet()", "createCommandHelper_column()","createCommandHelper_finalize()"]
  },
  "plotBarChart": {
    description: "Display a bar-chart with a count of unique IDs under each term in a column.",
    steps: ["createCommandHelper_sheet()", "createCommandHelper_column()","createCommandHelper_finalize()"]
  },
  "getN": {
    description:"Get the number of unique IDs ",
    steps: ["createCommandHelper_finalize()"]
  },
  "getIDs": {
    description:"Get a list of all unique IDs",
    steps: ["createCommandHelper_finalize()"]
  },
  "getDataTerms": {
    description: "Get a count of unique IDs under each term in a column.",
    steps: ["createCommandHelper_sheet()", "createCommandHelper_column()","createCommandHelper_finalize()"]
  },
  "getOneVarStats": {
    description: "Get one variable statistics (mean, median, stddev, IQR, etc.) for a column.",
    steps: ["createCommandHelper_sheet()", "createCommandHelper_column()","createCommandHelper_finalize()"]
  },
  "getMatches": {
    description: "Get a list of all unique IDs that match a term in a column.",
    steps: ["createCommandHelper_sheet()", "createCommandHelper_column()", "createCommandHelper_term()","createCommandHelper_finalize()"]
  },
  "getNumMatches": {
    description: "Get the total number of unique IDs that match a term in a column.",
    steps: ["createCommandHelper_sheet()", "createCommandHelper_column()", "createCommandHelper_term()","createCommandHelper_finalize()"]
  },
  "getDrugStats": {
    description: "Get drug statistics for a specific drug. Includes max dosages, first reaction outcomes, distribution of days between first reaction and max dosage, etc.",
    steps: [
      "createCommandHelper_column({sheetName: 'Drugs', title: 'Select drug categorization (you probably want active ingredient)', defaultButton: 'btn-outline-danger', preferredValues: ['WHODrug active ingredient', 'WHODrug trade name', 'Reported medication']})",
      "createCommandHelper_term({title: 'Enter the dataterm to generate drug stats for', sheetName: 'Drugs'})",
      "createCommandHelper_content({buttons: {'Include Days List':'If selected, the output will contain a list of the days from max dose to reaction. This list could be long. It will contain a descriptive statistics summary.', 'Exclude Days List':'If selected, the output will not contain a list of the days from max dose to reaction. It will contain a descriptive statistics summary.'}})",
      "createCommandHelper_finalize()"
    ]
  },
  "getOverlapsWithinDatapoint": {
    description: "Outputs a keyed-matrix with counts of the unique ID overlaps for all term combinations in a column. (Warning: could be slow for columns with a large number of unique terms)",
    steps: ["createCommandHelper_sheet()", "createCommandHelper_column()","createCommandHelper_finalize()"]
  },
  "getDataPoints": {
    description: "Outputs all available sheets & columns, and whether or not the column contains multiple dataterms belonging to the same ID (this occurs when a sheet may have multiple entries per ID)",
    steps: ["createCommandHelper_sheet()", "createCommandHelper_column()","createCommandHelper_finalize()"]
  }
}

//Future TODO: merge the core of these 3 functions
function createCommandHelper_sheet(){
  let ret = "<h5>Please Select an Available Sheet</h5>"
  let next_stage = createCommandHelper_history.length + 2
  function getButton(value, btnType = undefined){
    if( value.indexOf('Custom') === 0) btnType = "btn-outline-dark"
    if(!btnType) btnType = "btn-outline-success";
    return `<button type="button" data-dismiss="modal" data-target="#createCommandHelperModal" class="btn ${btnType} buttonpadding" onclick='createCommandHelper_history.push("${value}");createCommandHelper(stage = ${next_stage})'>${value}</button>`
  }
  for(let sheetName in eD.iterableKeys){
    ret += getButton(sheetName);
  }
  return ret;
};


let createCommandHelper_filter = undefined;
let createCommandHelper_history = undefined;
let createCommandHelper_command = undefined;

function createCommandHelper_column(args = {}){
  let ret = "<h5>Please Select an Available Column</h5>"
  if (args.title) ret = `<h5>${args.title}</h5>`

  let next_stage = createCommandHelper_history.length + 2
  if(args.sheetName) sheetName = args.sheetName
  else sheetName = createCommandHelper_history[next_stage-3];


  let defaultButton = "btn-outline-success"
  if(args.defaultButton) defaultButton = args.defaultButton;

  let preferredValues = [];
  if(args.preferredValues){
    for(let elem of args.preferredValues) preferredValues.push(elem);
  }
  //console.log(args, preferredValues);

  //console.log(args, ret, sheetName, defaultButton, preferredValues);
  function getButton(value, btnType = undefined){
    if( value.indexOf('Custom') === 0) btnType = "btn-outline-dark"
    if(!btnType) btnType = defaultButton;
    if(preferredValues.includes(value)) return `<button type="button" data-dismiss="modal" data-target="#createCommandHelperModal" class="btn btn-outline-success buttonpadding" onclick='createCommandHelper_history.push("${value}");createCommandHelper(stage = ${next_stage})'>${value}</button>`
    return `<button type="button" data-dismiss="modal" data-target="#createCommandHelperModal" class="btn ${btnType} buttonpadding" onclick='createCommandHelper_history.push("${value}");createCommandHelper(stage = ${next_stage})'>${value}</button>`
  }
  for(let colName in eD.iterableKeys[sheetName]){
    ret += getButton(colName);
  }
  //console.log(ret);
  return ret;
};

function createCommandHelper_term(args = {}){
  let ret = "<h5>Enter your dataterm (search term)</h5>";
  if(args.title) ret = `<h5>${args.title}</h5>`
  let next_stage = createCommandHelper_history.length + 2;
  let sheetName;
  if(args.sheetName) sheetName = args.sheetName;
  else sheetName = createCommandHelper_history[next_stage-4];
  let colName = createCommandHelper_history[next_stage-3];

  ret += '<div class="input-group mb-3">';
    ret += '<input type="text" class="form-control" placeholder="type your term here" id="createCommandHelper_inputTerm"><div class="input-group-append">';
    ret += `<button onclick='createCommandHelper_history.push(document.getElementById("createCommandHelper_inputTerm").value); createCommandHelper(stage = ${next_stage})' class="btn btn-success" type="submit">Submit</button>`;
    ret += '</div>';
  ret += '</div>';

  function getButton(term, count, btnType = undefined){
    if(!btnType) btnType = "btn-outline-success";
    return `<button type="button" data-dismiss="modal" data-target="#createCommandHelperModal" class="btn ${btnType} buttonpadding" onclick='createCommandHelper_history.push("${term}");createCommandHelper(stage = ${next_stage})'>${term} (${count})</button>`
  }
  ret += "<h5>...or select from these found dataterms</h5>";
  let dT = eD.getDataTerms(sheetName, colName);
  for(let property in dT){
    ret += getButton(property, dT[property]);
  }
  return ret;
}

function createCommandHelper_content(args = {}){
  let ret = "<h5>Please Select an option for this command's output</h5>"
  if (args.title) ret = `<h5>${args.title}</h5>`

  let next_stage = createCommandHelper_history.length + 2

  let defaultButton = "btn-outline-success"
  if(args.defaultButton) defaultButton = args.defaultButton;

  let preferredValues = [];
  if(args.preferredValues){
    for(let elem of args.preferredValues) preferredValues.push(elem);
  }

  function getButton(value, btnType = undefined){
    if(!btnType) btnType = defaultButton;
    if(preferredValues.includes(value)) return `<button type="button" data-dismiss="modal" data-target="#createCommandHelperModal" class="btn btn-outline-success buttonpadding" onclick='createCommandHelper_history.push("${value}");createCommandHelper(stage = ${next_stage})'>${value}</button>`
    return `<button type="button" data-dismiss="modal" data-target="#createCommandHelperModal" class="btn ${btnType} buttonpadding" onclick='createCommandHelper_history.push("${value}");createCommandHelper(stage = ${next_stage})'>${value}</button>`
  }
  function getOption(optionName){
    let nhm = '<hr><div class="row">';
    nhm += `<div class="col-auto align-middle">${getButton(optionName)}</div>`;
    nhm += `<div class="col">${args.buttons[optionName]}</div>`;
    nhm += '</div>';
    return nhm;
  }

  for(let property in args.buttons) ret += getOption(property)
  return ret;
}

function createCommandHelper_finalize(){
  createCommandHelper_command = ""
  if(createCommandHelper_filter) createCommandHelper_command += createCommandHelper_filter + ": "

  createCommandHelper_command += createCommandHelper_history[0];
  for(let i = 1; i < createCommandHelper_history.length; i++){
    createCommandHelper_command += ' "';
    createCommandHelper_command += createCommandHelper_history[i];
    createCommandHelper_command += '"'
  }

  //console.log(createCommandHelper_command);

  let nMB = "<h5>Your Command</h5>"
  nMB += `<p class="text-center bg-dark text-light rounded" style="font-family: 'Courier New'">${createCommandHelper_command}</p>`

  nMB += `<button onclick='navigator.clipboard.writeText(createCommandHelper_command)' class="btn btn-primary text-right">Copy to Clipboard</button>`

  let nF = "";
  nF += `<button type="button" data-dismiss="modal" data-target="#createCommandHelperModal" class="btn btn-danger">Close (Don\'t Add Command)</button>`
  nF += `<button type="button" data-dismiss="modal" data-target="#createCommandHelperModal" class="btn btn-success" onclick='addtoAvailableCommands()'>Add Command and Close</button>`
  document.getElementById('createCommandHelperModal_footer').innerHTML = nF;
  return nMB;
}

function addtoAvailableCommands(){
  document.getElementById('inputOnPage').value += '\n' + createCommandHelper_command;
}

function createCommandHelper(stage = 0){
  //console.log(stage, createCommandHelper_history);
  let cMB = document.getElementById('createCommandHelperModal_body');
  let nCMB = "";
  function getCommand(commandName){
    let nhm = '<div class="row">';
    let button = getButton(`onclick='createCommandHelper_history.push("${commandName}");createCommandHelper(stage = 2)'`, commandName)
    nhm += `<div class="col align-middle" style="padding:0.5em">${createCommandHelper_dict[commandName].description}</div>`;
    nhm += `<div class="col-auto">${button}</div>`;
    nhm += '</div><hr>';
    return nhm;
  }
  function getButton(attributes, value, btnType = undefined){
    if(!btnType) btnType = "btn-outline-success";
    if( value.indexOf('Custom') === 0) btnType = "btn-outline-dark"
    return `<button type="button" data-dismiss="modal" data-target="#createCommandHelperModal" class="btn ${btnType} buttonpadding" ${attributes}'>${value}</button>`
  }
  if (stage === 0){
    let lMBf = document.getElementById('createCommandHelperModal_footer')
    lMBf.innerHTML ='<button type="button" data-dismiss="modal" data-target="#createCommandHelperModal" class="btn btn-danger">Cancel</button>';

    createCommandHelper_filter = undefined;
    nCMB += "<h5>Will this command run against the entire dataset?</h5>";
    nCMB += getButton('onclick="createCommandHelper(stage = 1)"', "Use all the data", "btn-outline-primary");


    nCMB += "<br/><br/> <h5>...or do you want to use a filter for this command?</h5>";
    let filters = getFilters(removeComments(document.getElementById('inputOnPage').value.split('\n')))
    for(let property in filters){
      nCMB += getButton(`onclick='createCommandHelper_filter="${property}";createCommandHelper(stage = 1)'`, property);
    }
  }
  else if (stage === 1){
    createCommandHelper_history = [];
    nCMB =createCommandHelper_filter;
    nCMB = "<h5>Select a command</h5>";
    nCMB += '<div class="container-fluid">';
    for(let property in createCommandHelper_dict){
      nCMB += getCommand(property);
    }
    nCMB = nCMB.slice(0,-4);
    nCMB += '</div>'
  }
  else if (stage >= 2){
    nCMB = eval(createCommandHelper_dict[createCommandHelper_history[0]].steps[stage-2])
  }
  cMB.innerHTML = nCMB;

  //Goddamn is this useless but it looks so cool when its enabled
  //console.log(nCMB);

  $('#createCommandHelperModal').modal('show');
}


document.addEventListener("DOMContentLoaded", function(){
      if (!console) {
          console = {};
      }

      //Progress Log
      var logger = document.getElementById('consoleOnPage');

      //Redirect console output --> Progress Log
      console.log = function () {
          let counter = 0
          output = ""
          for(let message of Object.values(arguments)){
            if(counter > 0) output += ' ';
            if (typeof message === 'object') {
                output += (JSON && JSON.stringify ? JSON.stringify(message) : String(message));
            }
            else if (typeof message === 'string') {
              if(message.indexOf('\n') >= 0){
               for(let pmessage of message.split('\n')){
                output += pmessage;
                output +=  '<br />';
                }
              }
              else output += message;
            }
            else {
                output += message;
            }
            counter += 1
          }
          logger.innerHTML += output;
          logger.innerHTML +=  '<br />';
      }

      //Redirect captured errors --> Progress Log
      console.error = function () {
        let counter = 0
        errorOutput = "<span class='bg-danger'>"
        let toast = true;
        for(let message of Object.values(arguments)){
          if(counter > 0) errorOutput += ' ';
          if (typeof message === 'object') {
              errorOutput += (JSON && JSON.stringify ? JSON.stringify(message) : String(message));
          } else {
              errorOutput += message;
              if (message === 'Caught exceptions will be highlighted' ) toast = false;
          }
          counter += 1
        }
        errorOutput +=  `</span>`;
        logger.innerHTML += errorOutput;
        logger.innerHTML +=  '<br />'
        if(toast) {
          $('#notifyError').toast({delay: 5000});
          $('#notifyError').toast('show');
        }
      }

      //Initialize Progress Log
      initPageConsole()

      //Initialize File Loading Management
      new LoadFile({
        inputElement: document.getElementById('inputfile'),
        dropElement: document.getElementById('dropfile')
      });

      //
      initInputTemplate()
      clearTextArea('outputOnPage');
});
