//let eD=null
onmessage = function(event) {
  console.log('Message Receieved', event)
  //vigiLoadWorkbook(event)
  console.log('Posting Message')
  postMessage('messagePost')
}
function vigiLoadWorkbook(event){
  showLoadingModal();
  event.stopPropagation(); event.preventDefault();
  if (!event || !event.target || !event.target.files || event.target.files.length === 0) return;
  const filename = event.target.files[0].name;
  //document.getElementById('vigiLoadWorkbook_label').textArea
  document.getElementById('vigiLoadWorkbook_label').innerHTML = filename
  //readSingleFile(event);
  try{
    readExcelFile(event);
  }
  catch(err){
    handleError(err)
  }
}

function readExcelFile(event){
  let file = event.target.files[0];
  if(!file) return;

  let reader = new FileReader();
  reader.onloadstart = function(anEvent){
  }
  reader.onload = function(anEvent){
    try{
      var data = new Uint8Array(anEvent.target.result);
      var workbook = XLSX.read(data, {type: 'array'});
      eD = new VigiWrapper(workbook,['Cases','Drugs','Reactions'], 'UMC report ID');
      hideLoadingModal();
      testExcelFile();
    }
    catch(err){
      handleError(err)
    }
  };
}

function readSingleFile(event){
  let file = event.target.files[0];
  if(!file) return;

  let reader = new FileReader();
  reader.onload = function(anEvent){
    var contents = anEvent.target.result;
    console.log(contents);
  };
  reader.readAsText(file);
}
