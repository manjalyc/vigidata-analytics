/*
ComplexSet is a powerful extension of the Set class
ComplexSet implements a complement mode which indicates that the stored members are not actually in the set
	- the complementMode variable is used to denote this state
Consequently ComplexSet is able to uniquely implement:
	- Support for the Absolute (Universal) complement, even if the Universal Set is unknown.
In addition to the Absolute Complement, ComplexSet implements:
	- Full support for all set operations, even when one or more sets are in complement mode
		- Union
		- Intersection
		- Difference
		- Symmetric Difference
	- Support for the Relative Complement*
		* note unless explcitly stated otherwise all complements are the Proper Absolute Complements
	- Copy support*
		* note if elements are not primitives a copy will not recreate/deepcopy the elements
This Code is free software, licensed under the LGPLv3
*/
class ComplexSet extends Set{
	complementMode = false;

	static getCopy(ComplexSetA){
		return ComplexSetA.getCopy();
	}

	static complement(ComplexSetA){
		let copy = ComplexSetA.getCopy();
		copy.complementUpdate();
		return copy;
	}

	static relativeComplement(ComplexSetA, ComplexSetB){
		let copy = ComplexSetA.getCopy();
		copy.relativeComplementUpdate(ComplexSetB);
		return copy;
	}

	static union(ComplexSetA, ComplexSetB){
		let copy = ComplexSetA.getCopy();
		copy.unionUpdate(ComplexSetB);
		return copy;
	}

	static intersection(ComplexSetA, ComplexSetB){
		let copy = ComplexSetA.getCopy();
		copy.intersectionUpdate(ComplexSetB);
		return copy;
	}

	static difference(ComplexSetA, ComplexSetB){
		let copy = ComplexSetA.getCopy();
		copy.differenceUpdate(ComplexSetB);
		return copy;
	}

	static symmetricDifference(ComplexSetA, ComplexSetB){
		let copy = ComplexSetA.getCopy();
		copy.symmetricDifferenceUpdate(ComplexSetB);
		return copy;
	}

	getCopy(){
		let copy = new ComplexSet(this);
		copy.complementMode = this.complementMode;
		return copy
	}

	complementUpdate(){
		this.complementMode = !this.complementMode;
	}

	relativeComplementUpdate(ComplexSetB){
		let newEleCS = this.constructor.difference(ComplexSetB, this);
		this.clear()
		this.complementMode = newEleCS.complementMode;
		newEleCS.forEach(elem => this.add(elem));
	}

	unionUpdate(ComplexSetB){
		if (!this.complementMode){
			//Case 1: (1,2) ⋃ (2,3)
			if (!ComplexSetB.complementMode){
				for(let elem of ComplexSetB) this.add(elem);
			}
			//Case 2: (1,2) ⋃ (2,3)'
			else{
				let newEleCS = [];
				for(let elem of ComplexSetB){
					if(!this.has(elem)) newEleCS.push(elem);
				}
				this.clear();
				this.complementMode = true;
				newEleCS.forEach(elem => this.add(elem));
			}
		}
		else {
			//Case 3: (1,2)' ⋃ (2,3)
			if (!ComplexSetB.complementMode){
				for(let elem of this.values()){
					if(ComplexSetB.has(elem)) this.delete(elem);
				}
			}
			//Case 4: (1,2)' ⋃ (2,3)'
			else {
				for(let elem of this){
					if(!ComplexSetB.has(elem)) this.delete(elem);
				}
			}
		}
	}

	intersectionUpdate(ComplexSetB){
		if (!this.complementMode){
			//Case 1: (1,2) ⋂ (2,3)
			if (!ComplexSetB.complementMode){
				for(let elem of this.values()){
					if (!ComplexSetB.has(elem)) this.delete(elem);
				}
			}
			//Case 2: (1,2) ⋂ (2,3)'
			else{
				for(let elem of this.values()){
					if (ComplexSetB.has(elem)) this.delete(elem);
				}
			}
		}
		else {
			//Case 3: (1,2)' ⋂ (2,3)
			if (!ComplexSetB.complementMode){
				let newEleCS = []
				for(let elem of ComplexSetB){
					if(!this.has(elem)) newEleCS.push(elem);
				}
				this.clear()
				this.complementMode = false
				newEleCS.forEach(elem => this.add(elem));
			}
			//Case 4: (1,2)' ⋂ (2,3)'
			else {
				for(let elem of ComplexSetB) this.add(elem);
			}
		}
	}

	differenceUpdate(ComplexSetB){
		if (!this.complementMode){
			//Case 1: (1,2) \ (2,3)
			if (!ComplexSetB.complementMode){
				for(let elem of ComplexSetB) this.delete(elem);
			}
			//Case 2: (1,2) \ (2,3)'
			else{
				for(let elem of this.values()){
					if(!ComplexSetB.has(elem)) this.delete(elem);
				}
			}
		}
		else {
			//Case 3: (1,2)' \ (2,3)
			if (!ComplexSetB.complementMode){
				for(let elem of ComplexSetB) this.add(elem);
			}
			//Case 4: (1,2)' \ (2,3)'
			else {
				let newEleCS = []
				for(let elem of ComplexSetB){
					if(!this.has(elem)) newEleCS.push(elem);
				}
				this.clear();
				this.complementMode = false;
				newEleCS.forEach(elem => this.add(elem));
			}
		}
	}

	symmetricDifferenceUpdate(ComplexSetB){
		/*
		//Simple (but wasteful) Implementation by a Property of the Symmetric Difference
		//Useful for checking all other operations (except relativeComplement)
		let intersect = this.constructor.intersection(ComplexSetB, this);
		this.unionUpdate(ComplexSetB);
		this.differenceUpdate(intersect);
		*/
		for(let elem of ComplexSetB){
			if (!this.has(elem)) this.add(elem);
			else this.delete(elem);
		}
		if (this.complementMode === ComplexSetB.complementMode) this.complementMode = false;
		else this.complementMode = true;

	}

  toStr(){
		let ret = `ComplexSet(${this.size}) [Set] { `
		for(let elem of this) ret += `${elem}, `
		ret += `complementMode: ${this.complementMode} }`
		//let ret = `{ (` + Array.from(this).join(', ') + `), complementMode: ${this.complementMode}}`
    return ret;
  }
}

//module.exports = ComplexSet

function tester(){
	let universalSet;
	let setA;
	let setB;
	let CS = ComplexSet;

	let reInit = function(){
		universalSet = new ComplexSet([1,2,3,4]);
		setA = new ComplexSet([1, 2]);
		setB = new ComplexSet([2, 3]);
	}
	reInit()

	console.log("Complements");
	console.log(CS.complement(setA));
	console.log(CS.complement(setB));

	/*
	Unions
	(1,2) ⋃ (2,3) => (1,2,3) | ComplexSet(3) [Set] { 1, 2, 3, complementMode: false }
	(1,2) ⋃ (2,3)' => (3)'   | ComplexSet(1) [Set] { 3, complementMode: true }
	(1,2)' ⋃ (2,3) => (1)'   | ComplexSet(1) [Set] { 1, complementMode: true }
	(1,2)' U (2,3)' => (2)'  | ComplexSet(1) [Set] { 2, complementMode: true }
	*/
	console.log("Unions")
	console.log(CS.union(setA, setB));
	console.log(CS.union(setA, CS.complement(setB)));
	console.log(CS.union(CS.complement(setA), setB));
	console.log(CS.union(CS.complement(setA), CS.complement(setB)));

	/*
	Intersections
	(1,2) ⋂ (2,3) => (2)        | ComplexSet(1) [Set] { 2, complementMode: false }
	(1,2) ⋂ (2,3)' => (1)       | ComplexSet(1) [Set] { 1, complementMode: false }
	(1,2)' ⋂ (2,3) => (3)       | ComplexSet(1) [Set] { 3, complementMode: false }
	(1,2)' ⋂ (2,3)' => (1,2,3)' | ComplexSet(3) [Set] { 1, 2, 3, complementMode: true }
	*/
	console.log("Intersections")
	console.log(CS.intersection(setA, setB));
	console.log(CS.intersection(setA, CS.complement(setB)));
	console.log(CS.intersection(CS.complement(setA), setB));
	console.log(CS.intersection(CS.complement(setA), CS.complement(setB)));

	/*
	Difference
	(1,2) \ (2,3) => (1)       | ComplexSet(1) [Set] { 1, complementMode: false }
	(1,2) \ (2,3)' => (2)      | ComplexSet(1) [Set] { 2, complementMode: false }
	(1,2)' \ (2,3) => (1,2,3)' | ComplexSet(3) [Set] { 1, 2, 3, complementMode: true }
	(1,2)' \ (2,3)' => (3)     | ComplexSet(1) [Set] { 3, complementMode: false }
	*/
	console.log("Difference")
	console.log(CS.difference(setA, setB));
	console.log(CS.difference(setA, CS.complement(setB)));
	console.log(CS.difference(CS.complement(setA), setB));
	console.log(CS.difference(CS.complement(setA), CS.complement(setB)));

	/*
	Symmetric Difference
	(1,2) Δ (2,3) => (1,3)   | ComplexSet(2) [Set] { 1, 3, complementMode: false }
	(1,2) Δ (2,3)' => (1,3)' | ComplexSet(2) [Set] { 1, 3, complementMode: true }
	(1,2)' Δ (2,3) => (1,3)' | ComplexSet(2) [Set] { 1, 3, complementMode: true }
	(1,2)' Δ (2,3)' => (3)   | ComplexSet(2) [Set] { 1, 3, complementMode: false }
	*/
	console.log("Symmetric Difference")
	console.log(CS.symmetricDifference(setA, setB));
	console.log(CS.symmetricDifference(setA, CS.complement(setB)));
	console.log(CS.symmetricDifference(CS.complement(setA), setB));
	console.log(CS.symmetricDifference(CS.complement(setA), CS.complement(setB)));

	/*
	Relative Complement
	(1,2) R (2,3) => (3)       | ComplexSet(1) [Set] { 3, complementMode: false }
	(1,2) R (2,3)' => (1,2,3)' | ComplexSet(3) [Set] { 2, 3, 1, complementMode: true }
	(1,2)' R (2,3) => (2)      | ComplexSet(1) [Set] { 2, complementMode: false }
	(1,2)' R (2,3)' => (1)     | ComplexSet(1) [Set] { 1, complementMode: false }
	*/
	console.log("Relative Complement")
	console.log(CS.relativeComplement(setA, setB));
	console.log(CS.relativeComplement(setA, CS.complement(setB)));
	console.log(CS.relativeComplement(CS.complement(setA), setB));
	console.log(CS.relativeComplement(CS.complement(setA), CS.complement(setB)));

}
//tester()
