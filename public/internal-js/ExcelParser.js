'use strict';
//const Statistics = require('./Statistics.js');
//const ComplexSet = require('./ComplexSet.js');
//const MappedComplexSet = require('./MappedComplexSet.js');

//barebones reading an Excel File into a cases array
class ExcelID {
	xl = XLSX
	cases = {};
	iterableKeys = {}; //indicates at least one datapoint is iterable
	IDcolumn;
	sheetsToProcess;

	constructor(workbook, sheetsToProcess, IDcolumn){
		this.sheetsToProcess = sheetsToProcess;
		this.IDcolumn = IDcolumn;
		this.addWorkbookToDict(workbook);
	}

	//add Data to a Datapoint, if Datapoint already exists it is added to an array
	//data is cast to a String
	//returns true if the datapoint already exists, false otherwise
	add(ID, sheetName, colName, data){
		data = String(data);

		if (colName in this.cases[ID][sheetName]){
			//backtrack to  make the data type consistent for this dataPoint across all IDs
			if(!this.iterableKeys[sheetName][colName]){
				for(let nID in this.cases){
					if (!(sheetName in this.cases[nID])) continue;
					if (!(colName in this.cases[nID][sheetName])) continue;
					this.cases[nID][sheetName][colName] = [this.cases[nID][sheetName][colName]]
				}
			}
			this.iterableKeys[sheetName][colName] = true;
			if (typeof(this.cases[ID][sheetName][colName]) !== 'object'){
				this.cases[ID][sheetName][colName] = [this.cases[ID][sheetName][colName]];
			}
			this.cases[ID][sheetName][colName].push(data);
			//console.log(this.cases[ID][sheetName][colName]);
			return true;
		}
		else if (this.iterableKeys[sheetName][colName]) {
			//make the data type consistent for this dataPoint across all IDs
			this.cases[ID][sheetName][colName] = [data];
			return true
		}
		this.cases[ID][sheetName][colName] = data;
		return false;
	}

	addWorkbookToDict(workbook){
		console.log("[INFO] Reading Workbook")
		//let workbook = this.xl.readFile(this.filename);
		let sheetNames = workbook.SheetNames;
		for (let sheetName of sheetNames){
			//Checkpoing: Adding Sheet
			if (!(this.sheetsToProcess.includes(sheetName))){
				console.log(`[INFO] Skipping sheet: ${sheetName}`);
				continue;
			}
			console.log(`[INFO] Processing sheet: ${sheetName}`);
			this.addSheetToDict(workbook, sheetName);
		}
		//Checkpoint: Available Keys
	}

	addSheetToDict(workbook, sheetName){
		//Get worksheet, raw for raw data, defvalue to not skip blank cells in json
		//undefined as it is its own type whereas null is an object like an array
		let ws = this.xl.utils.sheet_to_json(workbook.Sheets[sheetName], {raw: true, defval:''});
		//console.log(ws);
		//console.log(typeof(ws));

		//iterableKeys: initialize sheetName
		if(!(sheetName in this.iterableKeys)) this.iterableKeys[sheetName] = {};
		for (let colName in ws[0]){
			if(!(colName in this.iterableKeys));
				this.iterableKeys[sheetName][String(colName)] = false;
		}

		//Read Worksheet
		for(let entry of ws){
			let ID = entry[this.IDcolumn];
			if(ID == undefined) continue;

			//cases: initialize ID -> sheetName, track in iterableKeys
			if(!(ID in this.cases)) this.cases[ID] = {};
			if(!(sheetName in this.cases[ID])) this.cases[ID][sheetName] = {};

			//Add Entries
			//console.log(typeof(entry));
			for(let colName in entry){
				this.add(ID, sheetName, colName, entry[colName]);
			}
		}
	}

	getDataPoints(){
		return this.iterableKeys;
	}
}

class AdvancedFilterLogic{

	//acts directly on the filter object, does not create a copy
	static normalizeIDs(filter, cases, IDcolumn){
		let MS = ComplexSet;
		let MMS = MappedComplexSet;
		//console.log('Begin Normalization', filter[0].size, filter[1].size)
		//console.log('s', filter[0].size, filter[1].size);
		//force filter[0] (the IDs) out of complement mode
		if(filter[0].complementMode === true){
			let Universal = new ComplexSet(Object.keys(cases));
			//console.log('U', Universal.toStr());
			//console.log('F', filter[0].toStr())
			//console.log('B', MS.intersection(Universal, filter[0]).toStr())
			filter[0] = MS.intersection(Universal, filter[0]);
		}
		for(let [key, complexSet] of filter[1].entries()){
			//console.log('ss')
			//destructure sequenceKey (created by getSequenceKey) to get ID and sheetName
			let [ID, sheetName] = key.split(':');

			if(!filter[0].has(ID)){
				//console.log(ID, 'deleted', filter[0].toStr());
				filter[1].delete(key);
				continue;
			}

			//console.log(ID, sheetName,':', complexSet.toStr())
			//console.log(ID, complexSetSize, complexSet.complementMode)
			if(!complexSet.complementMode && complexSet.size === 0){
					//console.log(ID, key, complexSet.toStr())
					filter[0].delete(ID);
					//console.log('DeletedA', ID);
			}
			else if (complexSet.complementMode){
				if( typeof(cases[ID][sheetName][IDcolumn]) !==  'object'){
						if( complexSetSize === 1) filter[0].delete(ID);
						//console.log('DeletedB', ID);
				}
				else if (complexSet.unionsize === cases[ID][sheetName][IDcolumn].length){
					filter[0].delete(ID);
					//console.log('DeletedC', ID);
				}
			}
		}
		return filter
	}

	static complement(filter){
		let MS = ComplexSet;
		let MMS = MappedComplexSet;
		return [MS.complement(filter[0]), MMS.complement(filter[1])]
	}

	static union(filterA, filterB){
		let MS = ComplexSet;
		let MMS = MappedComplexSet;
		return [MS.union(filterA[0],filterB[0]), MMS.union(filterA[1],filterB[1])];
	}

	static intersection(filterA, filterB){
		let MS = ComplexSet;
		let MMS = MappedComplexSet;
		//console.log('Intersection Debug')
		//console.log(MS.intersection(filterA[0], filterB[0]).toStr())
		//console.log(MMS.intersection(filterA[1], filterB[1]).toStr())
		return [MS.intersection(filterA[0],filterB[0]), MMS.union(filterA[1],filterB[1])];
	}

	static difference(filterA, filterB){
		let MS = ComplexSet;
		let MMS = MappedComplexSet;
		return [MS.difference(filterA[0],filterB[0]), MMS.union(filterA[1],filterB[1])];
	}

	static symmetricDifference(filterA, filterB){
		let MS = ComplexSet;
		let MMS = MappedComplexSet;
		return [MS.symmetricDifference(filterA[0],filterB[0]), MMS.union(filterA[1],filterB[1])];
	}



	static _union(filterA, filterB){
		let MS = ComplexSet;
		let MMS = MappedComplexSet;
		return [MS.union(filterA[0],filterB[0]), MMS.union(filterA[1],filterB[1])];
	}

	static _intersection(filterA, filterB){
		let MS = ComplexSet;
		let MMS = MappedComplexSet;
		return [MS.intersection(filterA[0],filterB[0]), MMS.intersection(filterA[1],filterB[1])];
	}

	static _difference(filterA, filterB){
		let MS = ComplexSet;
		let MMS = MappedComplexSet;
		return [MS.difference(filterA[0],filterB[0]), MMS.difference(filterA[1],filterB[1])];
	}

	static _symmetricDifference(filterA, filterB){
		let MS = ComplexSet;
		let MMS = MappedComplexSet;
		return [MS.symmetricDifference(filterA[0],filterB[0]), MMS.symmetricDifference(filterA[1],filterB[1])];
	}
}

//generalized methods with Filtering by IDs or sheetSpecific Filtering without disturbing the ExcelID data
class FilteredExcelID extends ExcelID{
	IDs;
	mappedIDSequences; //key: `${ID}:${sheetName}:${colName}`, val: Set of blacklisted idxs

	constructor(workbook, sheetsToProcess, IDcolumn){
		super(workbook, sheetsToProcess, IDcolumn);
		this.clearFilters();
	}

	getIDs(){
		if(!this.IDs.complementMode) return this.IDs;
		let retIDs = new ComplexSet(Object.keys(this.cases));
		retIDs.intersectionUpdate(this.IDs);
		return retIDs
	}

	setIDs(IDs){
		this.IDs = new ComplexSet(IDs);
	}

	clearFilters(){
		this.IDs = new ComplexSet(Object.keys(this.cases));
		this.mappedIDSequences = new MappedComplexSet();
	}

	setFilter(filter){
		this.IDs = filter[0]
		this.mappedIDSequences = filter[1]
	}

	mergeFilter(filter){
		this.IDs.intersectionUpdate(filter[0]);
		this.mappedIDSequences.unionUpdate(filter[1]);
	}

	getCurrentFilterCopy(){
		return [this.IDs.getCopy(), this.mappedIDSequences.getCopy()]
	}


	getSequenceKey(ID, sheetName) {
		return `${ID}:${sheetName}`;
	}

	addSequenceKey(ID, sheetName, idx){
		let key = getSequenceKey(ID, sheetName);

		//initialize mapped ID sequence, and get complement mode
		let complementMode = false;
		if (!this.mappedIDSequences.has(key)) this.mappedIDSequences.set(key, new ComplexSet());
		else complementMode = this.mappedIDSequences(key).complementMode;

		if (!complementMode) this.mappedIDSequences.get(key).add(idx);
		else this.mappedIDSequences.get(key).delete(idx);
	}

	getSequence(ID, sheetName, colName){
		if(typeof(this.cases[ID][sheetName][colName]) === 'undefined') throw new Error(`Undefined Data while Sequencing @${ID} > ${sheetName} > ${colName}`);

		let sKey = this.getSequenceKey(ID, sheetName);
		let sequence;
		if(typeof(this.cases[ID][sheetName][colName]) !== 'object') sequence = new ComplexSet([0]);
		else sequence = new ComplexSet(new Array(this.cases[ID][sheetName][colName].length).keys()) //must always be a noncomplement set
		if(this.mappedIDSequences.has(sKey)){
			//console.log(sKey, sequence);
			sequence.intersectionUpdate(this.mappedIDSequences.get(sKey)); //will only return a noncomplement set
			//console.log(sKey, sequence);
		}
		return sequence
	}

	//data = dataterm = term, should probably rename this at some point
	sequenceData(ID, sheetName, colName, depthParse = true){
		let ret = [];
		if (!this.cases[ID] || !this.cases[ID][sheetName]){
			//console.error(`[README] The ID: '${ID}', failed to be sequenced under ${sheetName} > ${colName}`)
			throw new Error(`[README] The ID: '${ID}', failed to be sequenced under ${sheetName} > ${colName}`);
			return ret;
		}

		//console.log('a',ret, data, sheetName, colName,'|', this.cases[ID][sheetName]);

		let data = this.cases[ID][sheetName][colName];
		if(typeof(data) === 'undefined') throw new Error(`Undefined Data while Sequencing @${ID} > ${sheetName} > ${colName}`);
		if(typeof(data) !== 'object') data = [data];

		let sequence = this.getSequence(ID, sheetName, colName);
		sequence.forEach( (idx) => {
			if (depthParse && typeof(data[idx]) !== 'undefined'){
				let subdata = data[idx].replace(/\r\n/g,'\n');
				subdata = data[idx].replace(/\r/g,'\n');
				ret.push(...subdata.split('\n'));
			}
			else ret.push(data[idx]);
		});
		return ret;
	}

	getBasicUniversalFilter(sheetName, colName, searchTerm){
		let keepIDs = new ComplexSet();
		let mappedIDSequences = new MappedComplexSet();
		for(let ID of this.IDs){
			let data = this.sequenceData(ID, sheetName, colName, false);
			let sKey = this.getSequenceKey(ID, sheetName, colName);
			for(let i = 0; i < data.length; i++){
				if (data[i].toLowerCase() == searchTerm.toLowerCase()){
					keepIDs.add(ID)
					if( !mappedIDSequences.has(sKey)) mappedIDSequences.set(sKey, new ComplexSet());
					mappedIDSequences.get(sKey).add(i)
					continue
				}
			}
		}
		//console.log('kept', keepIDs.toStr())
		return [keepIDs, mappedIDSequences]
	}

}

class FilteredExcelWrapper extends FilteredExcelID{

	getN(){
		return this.getIDs().size;
	}

	getDataTerms(sheetName, colName){
		let ret = {};
		for(let ID of this.getIDs()){
			let alreadyCounted = new Set(); //to only get unique IDs
			for(let term of this.sequenceData(ID, sheetName, colName)){
				//console.log(term)
				if(alreadyCounted.has(term)) continue; //skip if term already counted for this ID
				if (!(term in ret)) ret[term] = 0;
				ret[term] += 1
				alreadyCounted.add(term);
			};
			//console.log(ID);
		}
		return ret;
	}

	//caseinsensitive, exact matches only
	getMatches(sheetName, colName, searchTerm){
		let ret = [];
		for(let ID of this.getIDs()){
			for(let term of this.sequenceData(ID, sheetName, colName)){
				if (term.toLowerCase() == searchTerm.toLowerCase()){
					ret.push(ID);
					break;
				}
			}
		}
		return ret;
	}

	//caseinsensitive, exact matches only
	getNumMatches(sheetName, colName, searchTerm){
		let ret = 0;
		for(let ID of this.getIDs()){
			for(let term of this.sequenceData(ID, sheetName, colName)){
				if (term.toLowerCase() == searchTerm.toLowerCase()){
					ret += 1;
					break;
				}
			}
		}
		return ret;
	}

	//sample-based statistics
	getOneVarStats(sheetName, colName){
		let data = []
		for(let ID of this.getIDs()){
			for(let term of this.sequenceData(ID, sheetName, colName)){
				if (!isNaN(term) && term != ''){
					data.push(Number(term))
					//console.log(term, Number(term))
				}
			}
		}
		return Statistics.oneVarStats(data);
	}

	getOverlapsWithinDatapoint(sheetName, colName){
		let ret = {};
		let termIDs = {};
		for(let ID of this.getIDs()){
			this.sequenceData(ID, sheetName, colName).forEach(term => {
				if (!(term in termIDs)){
					termIDs[term] = new ComplexSet();
					ret[term] = {}
				}
				//console.log(term, ID)
				termIDs[term].add(ID);
			});
		}
		//console.log(termIDs)
		let terms = Object.keys(termIDs)
		for(let i = 0; i < terms.length; i++){
			for(let j = i + 1; j < terms.length; j++){
				ret[terms[i]][terms[j]] = ComplexSet.intersection(termIDs[terms[i]], termIDs[terms[j]]).size;
				ret[terms[j]][terms[i]] = ret[terms[i]][terms[j]];
			}
		}
		return ret;
	}

}

//generates Vigibase data points
class CustomVigiDataPoints extends FilteredExcelWrapper{
	dataIntegrity;
	lastGeneratedDP = null;
	constructor(workbook, configuration){
		super(workbook, configuration.Sheets, configuration.UniqueIDidentifer);
		this.dataIntegrity = this.verifyDataIntegrity();
		try {
			if(configuration.generateCustomDataPoints) this.genCustomDataPoints();
		}
		catch(err){
			console.error('[README] An error occured generating custom data point:', this.lastGeneratedDP);
			console.error('[README] The following dataPoints were processed before the failure');
			console.error(JSON.stringify(this.getDataPoints(), null, '  '))
			console.error('Passing Error upstream')
			throw(err)
		}
		//for(let ID in this.cases) console.log(this.cases[ID]['Cases']['Custom|Continent of primary source']);
	}

	daysBetween(startDateString, endDateString){
		//only having the year is too vague, at least both strings need at least the month
		if (startDateString.length <= 4 || endDateString.length <= 4) return undefined;
		return Math.round((Date.parse(endDateString) - Date.parse(startDateString)) / 86400000);
	}

	verifyDataIntegrity(){
		console.log("[INFO] Checking Data Consistency")
		let ret = true;
		let delIDs = [];
		for(let ID in this.cases){
			for(let sheetName of this.sheetsToProcess){
				if(!this.cases[ID][sheetName]){
					delIDs.push(ID);
					ret = false;
					break
				}
			}
		}
		if(delIDs.length > 0){
			console.error('[README] Data Integrity Checks Failed for the following IDs');
			console.error('[README]', delIDs);
			console.error('[README] These IDs will be deleted from the processed dataset')
		}
		for(let ID of delIDs){
			delete this.cases[ID];
			this.IDs.delete(ID);
		}
		return ret;
	}

	genCustomDataPoints(){
		//ignore filters and force for all datapoints
		console.log("[INFO] Processing Custom Datapoints")
		this.iterableKeys['Custom'] = {}
		this.genCustomVigibaseInitialYear();
		this.genCustomContinentOfPrimarySource();
		this.genCustomReactionsDuration();
		this.genCustomDrugRoleCount();
		this.genCustomDrugDosePerDay();
	}

	//this function just helps deal with bad data, in an ideal world it would be unnecessary
	getFirstDatum(val){
		//console.log(val);
		if(typeof(val) === 'object'){
			return val[0];
		}
		//console.log(val)
		return val;
	}

	genCustomVigibaseInitialYear(){
		this.lastGeneratedDP = 'genCustomVigibaseInitialYear'
		this.iterableKeys['Cases']['Custom|VigiBase initial year'] = false;
		for(let ID in this.cases) {
			//console.log(ID);
			let year = this.getFirstDatum(this.cases[ID]['Cases']['VigiBase initial date']).substring(0,4);
			this.cases[ID]['Cases']['Custom|VigiBase initial year'] = year
		}
	}

	genCustomContinentOfPrimarySource(){
		this.lastGeneratedDP = 'genCustomContinentOfPrimarySource'
		this.iterableKeys['Cases']['Custom|Continent of primary source'] = false;
		let Americas = ['Canada', 'United States of America', 'Argentina', 'Brazil', 'Mexico', 'Colombia', 'Argentina', 'Canada', 'Peru', 'Venezuela', 'Chile', 'Ecuador', 'Guatemala', 'Cuba', 'Bolivia', 'Haiti', 'Dominican Republic', 'Honduras', 'Paraguay', 'Nicaragua', 'El Salvador', 'Costa Rica', 'Panama', 'Puerto Rico', 'Uruguay', 'Jamaica', 'Trinidad and Tobago', 'Guyana', 'Suriname', 'Bahamas', 'Belize', 'Barbados', 'Saint Lucia', 'Saint Vincent and the Grenadines', 'United States Virgin Islands', 'Grenada', 'Antigua and Barbuda', 'Dominica', 'Bermuda', 'Cayman Islands', 'Greenland', 'Saint Kitts and Nevis', 'Sint Maarten', 'Turks and Caicos Islands', 'Saint Martin', 'British Virgin Islands', 'Caribbean Netherlands', 'Anguilla', 'Saint Barthélemy', 'Saint Pierre and Miquelon', 'Montserrat', 'Falkland Islands'];
		let Europe = ['Austria', 'Belgium', 'Czechia', 'France', 'Germany', 'Greece', 'Italy', 'Norway', 'Poland', 'Slovakia', 'Spain', 'Switzerland', 'United Kingdom of Great Britain and Northern Ireland', 'Bulgaria', 'Denmark', 'Estonia', 'Russia', 'Germany', 'United Kingdom', 'France', 'Italy', 'Spain', 'Ukraine', 'Poland', 'Romania', 'Netherlands', 'Belgium', 'Czech Republic', 'Greece', 'Portugal', 'Sweden', 'Hungary', 'Belarus', 'Austria', 'Serbia', 'Switzerland', 'Bulgaria', 'Denmark', 'Finland', 'Slovakia', 'Norway', 'Ireland', 'Croatia', 'Moldova', 'Bosnia and Herzegovina', 'Albania', 'Lithuania', 'North Macedonia', 'Slovenia', 'Latvia', 'Estonia', 'Montenegro', 'Luxembourg', 'Malta', 'Iceland', 'Andorra', 'Monaco', 'Liechtenstein', 'San Marino', 'Holy See', 'New Zealand' ];
		let Australia = ['Australia'];
		let Asia = ['Japan', 'Korea (the Republic of)', 'China', 'India', 'Indonesia', 'Pakistan', 'Bangladesh', 'Japan', 'Philippines', 'Vietnam', 'Turkey', 'Iran', 'Thailand', 'Myanmar', 'South Korea', 'Iraq', 'Afghanistan', 'Saudi Arabia', 'Uzbekistan', 'Malaysia', 'Yemen', 'Nepal', 'North Korea', 'Sri Lanka', 'Kazakhstan', 'Syria', 'Cambodia', 'Jordan', 'Azerbaijan', 'United Arab Emirates', 'Tajikistan', 'Israel', 'Laos', 'Lebanon', 'Kyrgyzstan', 'Turkmenistan', 'Singapore', 'Oman', 'State of Palestine', 'Kuwait', 'Georgia', 'Mongolia', 'Armenia', 'Qatar', 'Bahrain', 'Timor-Leste', 'Cyprus', 'Bhutan', 'Maldives', 'Brunei', 'Taiwan', 'Hong Kong', 'Macao'];
		let Africa = ['Nigeria', 'Ethiopia', 'Egypt', 'DR Congo', 'Tanzania', 'South Africa', 'Kenya', 'Uganda', 'Algeria', 'Sudan', 'Morocco', 'Angola', 'Mozambique', 'Ghana', 'Madagascar', 'Cameroon', 'Côte d\'Ivoire', 'Niger', 'Burkina Faso', 'Mali', 'Malawi', 'Zambia', 'Senegal', 'Chad', 'Somalia', 'Zimbabwe', 'Guinea', 'Rwanda', 'Benin', 'Burundi', 'Tunisia', 'South Sudan', 'Togo', 'Sierra Leone', 'Libya', 'Congo', 'Liberia', 'Central African Republic', 'Mauritania', 'Eritrea', 'Namibia', 'Gambia', 'Botswana', 'Gabon', 'Lesotho', 'Guinea-Bissau', 'Equatorial Guinea', 'Mauritius', 'Eswatini', 'Djibouti', 'Comoros', 'Cabo Verde', 'Sao Tome & Principe', 'Seychelles'];
		let Antartica = ['Antartica'];
		for(let ID in this.cases) {
			let country = this.getFirstDatum(this.cases[ID]['Cases']['Country of primary source']);
			let continent = `Unknown | ${country}`
			if (Americas.includes(country)) continent = 'Americas';
			else if (Europe.includes(country)) continent = 'Europe';
			else if (Asia.includes(country)) continent = 'Asia';
			else if (Africa.includes(country)) continent = 'Africa';
			else if (Australia.includes(country)) continent = 'Australia';
			else if (Antartica.includes(country)) continent = 'Antartica';
			this.cases[ID]['Cases']['Custom|Continent of primary source'] = continent;
		}
		//for(let ID in this.cases) console.log(this.cases[ID]['Cases']['Custom|Continent of primary source']);
	}

	genCustomReactionsDuration(){
		this.lastGeneratedDP = 'genCustomReactionsDuration'
		this.iterableKeys['Reactions']['Custom|Duration (days)'] = true;
		for(let ID in this.cases) {
			this.cases[ID]['Reactions']['Custom|Duration (days)'] = []
			//console.log(this.cases[ID]['Reactions']['Start date']);
			let startDates = this.cases[ID]['Reactions']['Start date'];
			let endDates = this.cases[ID]['Reactions']['End date'];
			for(let i = 0; i < startDates.length; i++){
				this.cases[ID]['Reactions']['Custom|Duration (days)'].push(String(this.daysBetween(startDates[i], endDates[i])));
				//if(startDates[i].length > 1) console.log('SD', startDates[i], endDates[i])
			}
		}
	}

	genCustomDrugRoleCount(){
		this.lastGeneratedDP = 'genCustomDrugRoleCount'
		this.iterableKeys['Drugs']['Custom|Total Suspect'] = false;
		this.iterableKeys['Drugs']['Custom|Total Concomitant'] = false;
		this.iterableKeys['Drugs']['Custom|Total Interacting'] = false;
		for(let ID in this.cases) {
			this.cases[ID]['Drugs']['Custom|Total Suspect'] = 0;
			this.cases[ID]['Drugs']['Custom|Total Concomitant'] = 0;
			this.cases[ID]['Drugs']['Custom|Total Interacting'] = 0;
			for(let role of this.cases[ID]['Drugs']['Role']){
				if(role === 'Suspect') this.cases[ID]['Drugs']['Custom|Total Suspect']++;
				else if(role === 'Concomitant') this.cases[ID]['Drugs']['Custom|Total Concomitant']++;
				else if(role === 'Interacting') this.cases[ID]['Drugs']['Custom|Total Interacting']++;
			}
			this.cases[ID]['Drugs']['Custom|Total Suspect'] = String(this.cases[ID]['Drugs']['Custom|Total Suspect']);
			this.cases[ID]['Drugs']['Custom|Total Concomitant'] = String(this.cases[ID]['Drugs']['Custom|Total Concomitant']);
			this.cases[ID]['Drugs']['Custom|Total Interacting'] = String(this.cases[ID]['Drugs']['Custom|Total Interacting']);
		}
	}

	genCustomDrugDosePerDay(){
		this.lastGeneratedDP = 'genCustomDrugDosePerDay'
		let tmpterms = new Set()
		this.iterableKeys['Drugs']['Custom|Dose (per Day)'] = true;
		for(let ID in this.cases) {
			this.cases[ID]['Drugs']['Custom|Dose (per Day)'] = []
			for(let i = 0; i < this.cases[ID]['Drugs']['Dose'].length; i++){
				let dailyDose = this.cases[ID]['Drugs']['Dose'][i];
				let doseFreq = this.cases[ID]['Drugs']['Dosage regimen'][i]
				if (dailyDose === ''){
					if (doseFreq === ''){
						//console.log('skip');
						this.cases[ID]['Drugs']['Custom|Dose (per Day)'].push('');
						continue
					}
					dailyDose = '1';
				}
				dailyDose = Number(dailyDose)
				doseFreq = doseFreq.toLowerCase()
				.replace(/‒/g,'-')
				.replace('hours','hour')
				.replace('days','day')
				.replace('weeks','week')
				.replace('months','month')
				.replace('years','year')
				.replace('- -','1 day')
				.replace('-','1')

				if (doseFreq === '' || doseFreq === '1 per 1 day'){
					this.cases[ID]['Drugs']['Custom|Dose (per Day)'].push(String(dailyDose));
					continue
				}

				doseFreq = doseFreq.split(' ');
				if (doseFreq.includes('hour')) dailyDose = dailyDose * 24;
				else if (doseFreq.includes('week')) dailyDose = dailyDose / 7;
				else if (doseFreq.includes('month')) dailyDose = dailyDose / 30;
				else if (doseFreq.includes('year')) dailyDose = dailyDose / 365;

				if (doseFreq.includes('cyclical') || doseFreq.includes('total') || doseFreq.includes('necessary')){
					this.cases[ID]['Drugs']['Custom|Dose (per Day)'].push('');
					continue;
				}
				tmpterms.add(doseFreq[doseFreq.length-1])

				if (doseFreq.length === 4) {
					dailyDose = dailyDose * Number(doseFreq[0])
					dailyDose = dailyDose / Number(doseFreq[2])
				}
				this.cases[ID]['Drugs']['Custom|Dose (per Day)'].push(String(dailyDose));
				//console.log(dailyDose.toFixed(2), '\t', this.cases[ID]['Drugs']['Dose'][i], '\t', this.cases[ID]['Drugs']['Dosage regimen'][i])
			}
		}
		//console.log(tmpterms);
	}
}

//internal Vigibase specific methods
class VigiMethods extends CustomVigiDataPoints{
	constructor(workbook, configuration){
		super(workbook, configuration);
	}

	getMaxDose(ID, drugColumn){
		let sequence = this.getSequence(ID, 'Drugs', drugColumn);
		let maxDoseIdx = -1;
		let maxDose = "";
		sequence.forEach( (idx) => {
			//console.log(maxDoseIdx, maxDose, idx, this.cases[ID]['Drugs']['Custom|Dose (per Day)'][idx])
			if(Number(this.cases[ID]['Drugs']['Custom|Dose (per Day)'][idx]) > Number(maxDose)){
				maxDoseIdx = idx;
				maxDose = Number(this.cases[ID]['Drugs']['Custom|Dose (per Day)'][idx]);
			}
		});
		if(!maxDose) maxDose = "Unknown";
		//if(maxDose === "100") console.log(900, ID);
		//if(maxDose === "900") console.log(900, ID);
		return [maxDoseIdx, maxDose];
	}

	getFirstReaction(ID, afterDate = ""){
		if(!afterDate) return [-1, -1];
		let sequence = this.getSequence(ID, 'Reactions', 'Start date');
		let reactionIdx = -1;
		let currDelt = -1;
		sequence.forEach( (idx) => {
			let delt = this.daysBetween(afterDate, this.cases[ID]['Reactions']['Start date'][idx] )
			if( delt > 0 ){
				if(currDelt === -1 || delt < currDelt){
					currDelt = delt;
					reactionIdx = idx;
				}
			}
		});
		return [reactionIdx, currDelt]
	}
}

class VigiWrapper extends VigiMethods{

	getDrugStats(drugColumn, term, includeDaysListStr){
		let daysList = true;
		if("Exclude Days List" === includeDaysListStr) daysList = false;

		let ret = {};
		let originalFilter = this.getCurrentFilterCopy();
		let universalDrugFilter = this.getBasicUniversalFilter('Drugs',drugColumn,term);

		let crossFilter = AdvancedFilterLogic.intersection(originalFilter, universalDrugFilter); //union subsequence
		crossFilter = AdvancedFilterLogic.normalizeIDs(crossFilter, this.cases, this.IDcolumn);
		this.setFilter(crossFilter);
		ret[`Total IDs with ${term}`] = this.getN();
		ret[`Total IDs with only ${term}`] = 0;
		ret[`Total IDs with ${term} + 1 other drug`] = 0;
		ret[`Total IDs with ${term} + >=2 other drugs`] = 0;
		for(let ID of this.getIDs()){
			count = this.sequenceData(ID, 'Drugs', drugColumn, term, false).length;
			if(count === 0) throw new Error('Something went wrong getting drug stat counts');
			else if(count === 1) ret[`Total IDs with only ${term}`] += 1;
			else if(count === 2) ret[`Total IDs with ${term} + 1 other drug`] += 1;
			else if(count > 2) ret[`Total IDs with ${term} + >=2 other drugs`] += 1
		}

		crossFilter = AdvancedFilterLogic._intersection(originalFilter, universalDrugFilter); //intersection subsequence
		crossFilter = AdvancedFilterLogic.normalizeIDs(crossFilter, this.cases, this.IDcolumn);
		this.setFilter(crossFilter);
		ret[`Max Dose of Drug (per Day)`] = {};
		ret[`First Reaction after Max Dose`] = {};
		ret[`First Reaction after Max Dose`]['Total Available Unique IDs'] = 0;
		ret[`First Reaction after Max Dose`]['Outcomes'] = {};
		ret[`First Reaction after Max Dose`]['Days from Max Dose to Reaction One Var Stats'] = {};
		ret[`First Reaction after Max Dose`]['Days from Max Dose to Reaction'] = [];

		for(let ID of this.getIDs()){
			let [maxDoseIdx, maxDose] = this.getMaxDose(ID, drugColumn);
			if(maxDose !== "Unknown") maxDose = String(maxDose) + " " + this.cases[ID]['Drugs']['Dose unit'][maxDoseIdx];
			if(!ret[`Max Dose of Drug (per Day)`][maxDose]) ret[`Max Dose of Drug (per Day)`][maxDose] = 0;
			ret[`Max Dose of Drug (per Day)`][maxDose] += 1;

			let [rIdx, rDays] = this.getFirstReaction(ID, this.cases[ID]['Drugs']['Start date'][maxDoseIdx]);
			if(rIdx !== -1){
				ret[`First Reaction after Max Dose`]['Total Available Unique IDs'] += 1;

				let outcome = this.cases[ID]['Reactions']['Outcome'][rIdx]
				if(!ret[`First Reaction after Max Dose`]['Outcomes'][outcome]) ret[`First Reaction after Max Dose`]['Outcomes'][outcome] = 0
				ret[`First Reaction after Max Dose`]['Outcomes'][outcome] += 1

				ret[`First Reaction after Max Dose`]['Days from Max Dose to Reaction'].push(rDays)
			}
		}

		if(ret[`First Reaction after Max Dose`]['Total Available Unique IDs'] > 2)
			ret[`First Reaction after Max Dose`]['Days from Max Dose to Reaction One Var Stats'] = Statistics.oneVarStats(ret[`First Reaction after Max Dose`]['Days from Max Dose to Reaction']);

		if(!daysList) delete ret[`First Reaction after Max Dose`]['Days from Max Dose to Reaction'];
		return ret;
	}


}
