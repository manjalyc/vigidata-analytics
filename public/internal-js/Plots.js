var pM;
var pW;

function testPlots(){
  try{
    pM.clearPlots();
    PlotCreator.plotPieChart('Cases', 'Country of primary source');
    PlotCreator.plotBarChart('Cases', 'Custom|Continent of primary source');
    PlotCreator.plotDoughnotChart('Cases', 'Sex');
    pM.refreshPlots();
  }
  catch(err){
    handleError(err);
  }
  //PlotCreator.plotOneVarR(boxplotData);
}

class PlotManager {
  divID = null;
  allDicts = null;
  maxPerRow = 2;
  constructor(divID){
    this.divID = divID;
    this.allDicts = [];
  }

  setMaxPerRow(n){
    this.maxPerRow = n;
    this.refreshPlots();
  }

  addPlotNoUpdate(dict){
    this.allDicts.push(dict);
    console.log("[INFO]", `Loaded Plot #${this.allDicts.length} `);
  }

  addPlot(dict){
    this.addPlotNoUpdate(dict)
    this.refreshPlots();
    //this.plots.innerHTML += `<div class="container" id="Plot"><canvas id="${cID}"></canvas></div>`
    //for(let chart of this.charts) chart.update();
    //this.charts.push();
  }

  refreshPlots(){
    //console.log(this.divID)
    let plotsDiv = document.getElementById(this.divID);
    plotsDiv.innerHTML = "";

    //create div layout
    let currPlot = 0;
    let currRow = ''
    for(let i = 0; i < this.allDicts.length; i++){
      if(currPlot % this.maxPerRow === 0){
        currRow = ''
      }
      let cID = `${this.divID}${i}`

      currRow += `<div class="col container" id="Plot"><canvas id="${cID}"></canvas></div>`

      if(currPlot % this.maxPerRow === this.maxPerRow - 1){
        plotsDiv.innerHTML += '<div class="row">' + currRow + '</div>'
      }
      currPlot += 1
    }
    if(currRow !== '' && (currPlot % this.maxPerRow !== 0)) plotsDiv.innerHTML += '<div class="row">' + currRow + '</div>';

    //can't merge for loops
    for(let i = 0; i < this.allDicts.length; i++){
      let cID = `${this.divID}${i}`
      let ctx = document.getElementById(cID).getContext('2d');
      new Chart(ctx, this.allDicts[i])
    }
  }

  clearPlots(){
    console.log("[INFO]", 'Removed All Plots');
    this.allDicts = [];
    document.getElementById(this.divID).innerHTML = "";
  }
}

class ColorGenerator{
  bgColors = [
      'rgba(255, 99, 132, 0.2)',
      'rgba(54, 162, 235, 0.2)',
      'rgba(255, 206, 86, 0.2)',
      'rgba(75, 192, 192, 0.2)',
      'rgba(153, 102, 255, 0.2)',
      'rgba(255, 159, 64, 0.2)'
  ];
  colorIdx;

  constructor(){
    this.colorIdx = -1;
  }

  getNextColor(){
    this.colorIdx += 1;
    let n = this.bgColors.length;
    let bgColor = this.bgColors[this.colorIdx % n]
    let bdColor = bgColor.split(' ');
    bdColor[bdColor.length-1] = '1)';
    bdColor = bdColor.join(' ')
    return [bgColor, bdColor];
  }
}

class PlotCreator {
  //all datasets must have the label and data properties
  static addCountChart(sheetName, colName, type, filterName = null, refresh = true){
    let rawData = eD.getDataTerms(sheetName, colName);
    let dict = {
      type: type,
      data: {
        labels: [],
        datasets:[{
          label: "# of Unique IDs",
          borderWidth:1,
          hoverBorderWith:2,
          data:[],
          backgroundColor:[],
          borderColor:[]
        }]
      },
      options: {
        legend: {
          position: 'top'
         },
        title: {
          display: true,
          position: 'top',
          text: `${sheetName} by ${colName}`
        }
      }
    }

    if(type === 'pie' || type == 'doughnut') dict.options.legend.position = 'bottom';

    if(filterName) dict.options.title.text += `, under filter ${filterName}`

    let colors = new ColorGenerator;
    for(let [dataPoint, count] of Object.entries(rawData)){
      //console.log(dataPoint, count);
      if(dataPoint === '') dataPoint = 'Unknown';
      else if(!dataPoint) dataPoint = String(dataPoint);
      let [bgColor, bdColor] = colors.getNextColor();
      dict.data.datasets[0].data.push(count);
      dict.data.datasets[0].backgroundColor.push(bgColor);
      dict.data.datasets[0].borderColor.push(bdColor);
      dict.data.labels.push(dataPoint)
    }

    if(dict.data.labels.length > 25){
      dict.options.title.text += `, labels hidden (${dict.data.labels.length} labels)`
      dict.options.legend.display = false;
    }

    //console.log(dict)
    if(refresh) pM.addPlot(dict);
    else pM.addPlotNoUpdate(dict);
  }

  static plotPieChart(sheetName, colName, filterName = null, refresh = false){
    PlotCreator.addCountChart(sheetName, colName, 'pie', filterName = filterName, refresh = refresh);
  }

  static plotDoughnotChart(sheetName, colName, filterName = null, refresh = false){
    PlotCreator.addCountChart(sheetName, colName, 'doughnut', filterName = filterName, refresh = refresh);
  }

  static plotBarChart(sheetName, colName, filterName = null, refresh = false){
    PlotCreator.addCountChart(sheetName, colName, 'bar', filterName = filterName, refresh = refresh);
  }
}

document.addEventListener("DOMContentLoaded", function(){
  pM = new PlotManager('Plots');
})
