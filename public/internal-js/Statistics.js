'use strict'
//All Sample-based Statistics, not population

//linearly interpolated
//exclusive, expected value of rank order statistics
class Statistics{
	static quantiles(sorted, intervals) {
		let len = sorted.length;
		if (len < 2) return null;

		let ret = []
		for(let p = 1; p <  intervals; p += 1){
			let delt = p * (len+1) / intervals
			let idx = Math.floor(delt)
			delt -= idx
			if (idx < 1) idx = 1;
			else if(idx >= len) idx = len - 1;
			ret.push( sorted[idx - 1] * (1- delt) + sorted[idx] * (delt))
			//console.log(len, '\t', p, '\t', idx, '\t', delt, '\t', ret[ret.length-1])
		}
		return ret
	}

	static meanStdDev(data){
		let mean = data.reduce((a,b) => a + b)/data.length;
		let variance = 0
		data.forEach(elem => variance += Math.pow(elem-mean,2))
		variance = variance/(data.length);
		//console.log(mean, variance);
		return [mean, Math.pow(variance, 1/2)];
	}

	static oneVarStats(data){
		let ret = {
			n: data.length,
			quartile1: undefined,
			quartile3: undefined,
			median: undefined,
			mean: undefined,
			stddev: undefined, //sample standard deviation
			min: undefined,
			max: undefined,
			range: undefined
		};
		if (ret['n'] == 0) return ret;

		let sorted = data.sort( (a,b) => a-b);
		ret['min'] = sorted[0];
		ret['max'] = sorted[sorted.length - 1];
		ret['range'] = ret['max'] - ret['min'];
		if (ret['n'] == 1){
			ret['mean'] = sorted[0];
			ret['median'] = sorted[0];
			ret['stddev'] = 0;
			return ret;
		}
		[ret['quartile1'], ret['median'], ret['quartile3']] = Statistics.quantiles(sorted, 4);
		//console.log(sorted);
		[ret['mean'], ret['stddev']] = Statistics.meanStdDev(sorted);
		return ret;
	}
}

//module.exports = {oneVarStats}

//console.log('--',quantiles([1,2,3,4,5,6], 4))
//console.log(oneVarStats([1,2,3,4,5,6]))
//''.sad()
