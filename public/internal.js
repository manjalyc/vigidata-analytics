//This file combines all internal custom javascript into 1 file using Babel
//This file was automatically generated on Tue 30 Jun 2020 12:48:15 PM EDT
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
ComplexSet is a powerful extension of the Set class
ComplexSet implements a complement mode which indicates that the stored members are not actually in the set
	- the complementMode variable is used to denote this state
Consequently ComplexSet is able to uniquely implement:
	- Support for the Absolute (Universal) complement, even if the Universal Set is unknown.
In addition to the Absolute Complement, ComplexSet implements:
	- Full support for all set operations, even when one or more sets are in complement mode
		- Union
		- Intersection
		- Difference
		- Symmetric Difference
	- Support for the Relative Complement*
		* note unless explcitly stated otherwise all complements are the Proper Absolute Complements
	- Copy support*
		* note if elements are not primitives a copy will not recreate/deepcopy the elements
This Code is free software, licensed under the LGPLv3
*/
class ComplexSet extends Set {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "complementMode", false);
  }

  static getCopy(ComplexSetA) {
    return ComplexSetA.getCopy();
  }

  static complement(ComplexSetA) {
    let copy = ComplexSetA.getCopy();
    copy.complementUpdate();
    return copy;
  }

  static relativeComplement(ComplexSetA, ComplexSetB) {
    let copy = ComplexSetA.getCopy();
    copy.relativeComplementUpdate(ComplexSetB);
    return copy;
  }

  static union(ComplexSetA, ComplexSetB) {
    let copy = ComplexSetA.getCopy();
    copy.unionUpdate(ComplexSetB);
    return copy;
  }

  static intersection(ComplexSetA, ComplexSetB) {
    let copy = ComplexSetA.getCopy();
    copy.intersectionUpdate(ComplexSetB);
    return copy;
  }

  static difference(ComplexSetA, ComplexSetB) {
    let copy = ComplexSetA.getCopy();
    copy.differenceUpdate(ComplexSetB);
    return copy;
  }

  static symmetricDifference(ComplexSetA, ComplexSetB) {
    let copy = ComplexSetA.getCopy();
    copy.symmetricDifferenceUpdate(ComplexSetB);
    return copy;
  }

  getCopy() {
    let copy = new ComplexSet(this);
    copy.complementMode = this.complementMode;
    return copy;
  }

  complementUpdate() {
    this.complementMode = !this.complementMode;
  }

  relativeComplementUpdate(ComplexSetB) {
    let newEleCS = this.constructor.difference(ComplexSetB, this);
    this.clear();
    this.complementMode = newEleCS.complementMode;
    newEleCS.forEach(elem => this.add(elem));
  }

  unionUpdate(ComplexSetB) {
    if (!this.complementMode) {
      //Case 1: (1,2) ⋃ (2,3)
      if (!ComplexSetB.complementMode) {
        for (let elem of ComplexSetB) this.add(elem);
      } //Case 2: (1,2) ⋃ (2,3)'
      else {
          let newEleCS = [];

          for (let elem of ComplexSetB) {
            if (!this.has(elem)) newEleCS.push(elem);
          }

          this.clear();
          this.complementMode = true;
          newEleCS.forEach(elem => this.add(elem));
        }
    } else {
      //Case 3: (1,2)' ⋃ (2,3)
      if (!ComplexSetB.complementMode) {
        for (let elem of this.values()) {
          if (ComplexSetB.has(elem)) this.delete(elem);
        }
      } //Case 4: (1,2)' ⋃ (2,3)'
      else {
          for (let elem of this) {
            if (!ComplexSetB.has(elem)) this.delete(elem);
          }
        }
    }
  }

  intersectionUpdate(ComplexSetB) {
    if (!this.complementMode) {
      //Case 1: (1,2) ⋂ (2,3)
      if (!ComplexSetB.complementMode) {
        for (let elem of this.values()) {
          if (!ComplexSetB.has(elem)) this.delete(elem);
        }
      } //Case 2: (1,2) ⋂ (2,3)'
      else {
          for (let elem of this.values()) {
            if (ComplexSetB.has(elem)) this.delete(elem);
          }
        }
    } else {
      //Case 3: (1,2)' ⋂ (2,3)
      if (!ComplexSetB.complementMode) {
        let newEleCS = [];

        for (let elem of ComplexSetB) {
          if (!this.has(elem)) newEleCS.push(elem);
        }

        this.clear();
        this.complementMode = false;
        newEleCS.forEach(elem => this.add(elem));
      } //Case 4: (1,2)' ⋂ (2,3)'
      else {
          for (let elem of ComplexSetB) this.add(elem);
        }
    }
  }

  differenceUpdate(ComplexSetB) {
    if (!this.complementMode) {
      //Case 1: (1,2) \ (2,3)
      if (!ComplexSetB.complementMode) {
        for (let elem of ComplexSetB) this.delete(elem);
      } //Case 2: (1,2) \ (2,3)'
      else {
          for (let elem of this.values()) {
            if (!ComplexSetB.has(elem)) this.delete(elem);
          }
        }
    } else {
      //Case 3: (1,2)' \ (2,3)
      if (!ComplexSetB.complementMode) {
        for (let elem of ComplexSetB) this.add(elem);
      } //Case 4: (1,2)' \ (2,3)'
      else {
          let newEleCS = [];

          for (let elem of ComplexSetB) {
            if (!this.has(elem)) newEleCS.push(elem);
          }

          this.clear();
          this.complementMode = false;
          newEleCS.forEach(elem => this.add(elem));
        }
    }
  }

  symmetricDifferenceUpdate(ComplexSetB) {
    /*
    //Simple (but wasteful) Implementation by a Property of the Symmetric Difference
    //Useful for checking all other operations (except relativeComplement)
    let intersect = this.constructor.intersection(ComplexSetB, this);
    this.unionUpdate(ComplexSetB);
    this.differenceUpdate(intersect);
    */
    for (let elem of ComplexSetB) {
      if (!this.has(elem)) this.add(elem);else this.delete(elem);
    }

    if (this.complementMode === ComplexSetB.complementMode) this.complementMode = false;else this.complementMode = true;
  }

  toStr() {
    let ret = `ComplexSet(${this.size}) [Set] { `;

    for (let elem of this) ret += `${elem}, `;

    ret += `complementMode: ${this.complementMode} }`; //let ret = `{ (` + Array.from(this).join(', ') + `), complementMode: ${this.complementMode}}`

    return ret;
  }

} //module.exports = ComplexSet


function tester() {
  let universalSet;
  let setA;
  let setB;
  let CS = ComplexSet;

  let reInit = function () {
    universalSet = new ComplexSet([1, 2, 3, 4]);
    setA = new ComplexSet([1, 2]);
    setB = new ComplexSet([2, 3]);
  };

  reInit();
  console.log("Complements");
  console.log(CS.complement(setA));
  console.log(CS.complement(setB));
  /*
  Unions
  (1,2) ⋃ (2,3) => (1,2,3) | ComplexSet(3) [Set] { 1, 2, 3, complementMode: false }
  (1,2) ⋃ (2,3)' => (3)'   | ComplexSet(1) [Set] { 3, complementMode: true }
  (1,2)' ⋃ (2,3) => (1)'   | ComplexSet(1) [Set] { 1, complementMode: true }
  (1,2)' U (2,3)' => (2)'  | ComplexSet(1) [Set] { 2, complementMode: true }
  */

  console.log("Unions");
  console.log(CS.union(setA, setB));
  console.log(CS.union(setA, CS.complement(setB)));
  console.log(CS.union(CS.complement(setA), setB));
  console.log(CS.union(CS.complement(setA), CS.complement(setB)));
  /*
  Intersections
  (1,2) ⋂ (2,3) => (2)        | ComplexSet(1) [Set] { 2, complementMode: false }
  (1,2) ⋂ (2,3)' => (1)       | ComplexSet(1) [Set] { 1, complementMode: false }
  (1,2)' ⋂ (2,3) => (3)       | ComplexSet(1) [Set] { 3, complementMode: false }
  (1,2)' ⋂ (2,3)' => (1,2,3)' | ComplexSet(3) [Set] { 1, 2, 3, complementMode: true }
  */

  console.log("Intersections");
  console.log(CS.intersection(setA, setB));
  console.log(CS.intersection(setA, CS.complement(setB)));
  console.log(CS.intersection(CS.complement(setA), setB));
  console.log(CS.intersection(CS.complement(setA), CS.complement(setB)));
  /*
  Difference
  (1,2) \ (2,3) => (1)       | ComplexSet(1) [Set] { 1, complementMode: false }
  (1,2) \ (2,3)' => (2)      | ComplexSet(1) [Set] { 2, complementMode: false }
  (1,2)' \ (2,3) => (1,2,3)' | ComplexSet(3) [Set] { 1, 2, 3, complementMode: true }
  (1,2)' \ (2,3)' => (3)     | ComplexSet(1) [Set] { 3, complementMode: false }
  */

  console.log("Difference");
  console.log(CS.difference(setA, setB));
  console.log(CS.difference(setA, CS.complement(setB)));
  console.log(CS.difference(CS.complement(setA), setB));
  console.log(CS.difference(CS.complement(setA), CS.complement(setB)));
  /*
  Symmetric Difference
  (1,2) Δ (2,3) => (1,3)   | ComplexSet(2) [Set] { 1, 3, complementMode: false }
  (1,2) Δ (2,3)' => (1,3)' | ComplexSet(2) [Set] { 1, 3, complementMode: true }
  (1,2)' Δ (2,3) => (1,3)' | ComplexSet(2) [Set] { 1, 3, complementMode: true }
  (1,2)' Δ (2,3)' => (3)   | ComplexSet(2) [Set] { 1, 3, complementMode: false }
  */

  console.log("Symmetric Difference");
  console.log(CS.symmetricDifference(setA, setB));
  console.log(CS.symmetricDifference(setA, CS.complement(setB)));
  console.log(CS.symmetricDifference(CS.complement(setA), setB));
  console.log(CS.symmetricDifference(CS.complement(setA), CS.complement(setB)));
  /*
  Relative Complement
  (1,2) R (2,3) => (3)       | ComplexSet(1) [Set] { 3, complementMode: false }
  (1,2) R (2,3)' => (1,2,3)' | ComplexSet(3) [Set] { 2, 3, 1, complementMode: true }
  (1,2)' R (2,3) => (2)      | ComplexSet(1) [Set] { 2, complementMode: false }
  (1,2)' R (2,3)' => (1)     | ComplexSet(1) [Set] { 1, complementMode: false }
  */

  console.log("Relative Complement");
  console.log(CS.relativeComplement(setA, setB));
  console.log(CS.relativeComplement(setA, CS.complement(setB)));
  console.log(CS.relativeComplement(CS.complement(setA), setB));
  console.log(CS.relativeComplement(CS.complement(setA), CS.complement(setB)));
} //tester()
'use strict'; //const Statistics = require('./Statistics.js');
//const ComplexSet = require('./ComplexSet.js');
//const MappedComplexSet = require('./MappedComplexSet.js');
//barebones reading an Excel File into a cases array

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class ExcelID {
  //indicates at least one datapoint is iterable
  constructor(workbook, sheetsToProcess, IDcolumn) {
    _defineProperty(this, "xl", XLSX);

    _defineProperty(this, "cases", {});

    _defineProperty(this, "iterableKeys", {});

    _defineProperty(this, "IDcolumn", void 0);

    _defineProperty(this, "sheetsToProcess", void 0);

    this.sheetsToProcess = sheetsToProcess;
    this.IDcolumn = IDcolumn;
    this.addWorkbookToDict(workbook);
  } //add Data to a Datapoint, if Datapoint already exists it is added to an array
  //data is cast to a String
  //returns true if the datapoint already exists, false otherwise


  add(ID, sheetName, colName, data) {
    data = String(data);

    if (colName in this.cases[ID][sheetName]) {
      //backtrack to  make the data type consistent for this dataPoint across all IDs
      if (!this.iterableKeys[sheetName][colName]) {
        for (let nID in this.cases) {
          if (!(sheetName in this.cases[nID])) continue;
          if (!(colName in this.cases[nID][sheetName])) continue;
          this.cases[nID][sheetName][colName] = [this.cases[nID][sheetName][colName]];
        }
      }

      this.iterableKeys[sheetName][colName] = true;

      if (typeof this.cases[ID][sheetName][colName] !== 'object') {
        this.cases[ID][sheetName][colName] = [this.cases[ID][sheetName][colName]];
      }

      this.cases[ID][sheetName][colName].push(data); //console.log(this.cases[ID][sheetName][colName]);

      return true;
    } else if (this.iterableKeys[sheetName][colName]) {
      //make the data type consistent for this dataPoint across all IDs
      this.cases[ID][sheetName][colName] = [data];
      return true;
    }

    this.cases[ID][sheetName][colName] = data;
    return false;
  }

  addWorkbookToDict(workbook) {
    console.log("[INFO] Reading Workbook"); //let workbook = this.xl.readFile(this.filename);

    let sheetNames = workbook.SheetNames;

    for (let sheetName of sheetNames) {
      //Checkpoing: Adding Sheet
      if (!this.sheetsToProcess.includes(sheetName)) {
        console.log(`[INFO] Skipping sheet: ${sheetName}`);
        continue;
      }

      console.log(`[INFO] Processing sheet: ${sheetName}`);
      this.addSheetToDict(workbook, sheetName);
    } //Checkpoint: Available Keys

  }

  addSheetToDict(workbook, sheetName) {
    //Get worksheet, raw for raw data, defvalue to not skip blank cells in json
    //undefined as it is its own type whereas null is an object like an array
    let ws = this.xl.utils.sheet_to_json(workbook.Sheets[sheetName], {
      raw: true,
      defval: ''
    }); //console.log(ws);
    //console.log(typeof(ws));
    //iterableKeys: initialize sheetName

    if (!(sheetName in this.iterableKeys)) this.iterableKeys[sheetName] = {};

    for (let colName in ws[0]) {
      if (!(colName in this.iterableKeys)) ;
      this.iterableKeys[sheetName][String(colName)] = false;
    } //Read Worksheet


    for (let entry of ws) {
      let ID = entry[this.IDcolumn];
      if (ID == undefined) continue; //cases: initialize ID -> sheetName, track in iterableKeys

      if (!(ID in this.cases)) this.cases[ID] = {};
      if (!(sheetName in this.cases[ID])) this.cases[ID][sheetName] = {}; //Add Entries
      //console.log(typeof(entry));

      for (let colName in entry) {
        this.add(ID, sheetName, colName, entry[colName]);
      }
    }
  }

  getDataPoints() {
    return this.iterableKeys;
  }

}

class AdvancedFilterLogic {
  //acts directly on the filter object, does not create a copy
  static normalizeIDs(filter, cases, IDcolumn) {
    let MS = ComplexSet;
    let MMS = MappedComplexSet; //console.log('Begin Normalization', filter[0].size, filter[1].size)
    //console.log('s', filter[0].size, filter[1].size);
    //force filter[0] (the IDs) out of complement mode

    if (filter[0].complementMode === true) {
      let Universal = new ComplexSet(Object.keys(cases)); //console.log('U', Universal.toStr());
      //console.log('F', filter[0].toStr())
      //console.log('B', MS.intersection(Universal, filter[0]).toStr())

      filter[0] = MS.intersection(Universal, filter[0]);
    }

    for (let [key, complexSet] of filter[1].entries()) {
      //console.log('ss')
      //destructure sequenceKey (created by getSequenceKey) to get ID and sheetName
      let [ID, sheetName] = key.split(':');

      if (!filter[0].has(ID)) {
        //console.log(ID, 'deleted', filter[0].toStr());
        filter[1].delete(key);
        continue;
      } //console.log(ID, sheetName,':', complexSet.toStr())
      //console.log(ID, complexSetSize, complexSet.complementMode)


      if (!complexSet.complementMode && complexSet.size === 0) {
        //console.log(ID, key, complexSet.toStr())
        filter[0].delete(ID); //console.log('DeletedA', ID);
      } else if (complexSet.complementMode) {
        if (typeof cases[ID][sheetName][IDcolumn] !== 'object') {
          if (complexSetSize === 1) filter[0].delete(ID); //console.log('DeletedB', ID);
        } else if (complexSet.unionsize === cases[ID][sheetName][IDcolumn].length) {
          filter[0].delete(ID); //console.log('DeletedC', ID);
        }
      }
    }

    return filter;
  }

  static complement(filter) {
    let MS = ComplexSet;
    let MMS = MappedComplexSet;
    return [MS.complement(filter[0]), MMS.complement(filter[1])];
  }

  static union(filterA, filterB) {
    let MS = ComplexSet;
    let MMS = MappedComplexSet;
    return [MS.union(filterA[0], filterB[0]), MMS.union(filterA[1], filterB[1])];
  }

  static intersection(filterA, filterB) {
    let MS = ComplexSet;
    let MMS = MappedComplexSet; //console.log('Intersection Debug')
    //console.log(MS.intersection(filterA[0], filterB[0]).toStr())
    //console.log(MMS.intersection(filterA[1], filterB[1]).toStr())

    return [MS.intersection(filterA[0], filterB[0]), MMS.union(filterA[1], filterB[1])];
  }

  static difference(filterA, filterB) {
    let MS = ComplexSet;
    let MMS = MappedComplexSet;
    return [MS.difference(filterA[0], filterB[0]), MMS.union(filterA[1], filterB[1])];
  }

  static symmetricDifference(filterA, filterB) {
    let MS = ComplexSet;
    let MMS = MappedComplexSet;
    return [MS.symmetricDifference(filterA[0], filterB[0]), MMS.union(filterA[1], filterB[1])];
  }

  static _union(filterA, filterB) {
    let MS = ComplexSet;
    let MMS = MappedComplexSet;
    return [MS.union(filterA[0], filterB[0]), MMS.union(filterA[1], filterB[1])];
  }

  static _intersection(filterA, filterB) {
    let MS = ComplexSet;
    let MMS = MappedComplexSet;
    return [MS.intersection(filterA[0], filterB[0]), MMS.intersection(filterA[1], filterB[1])];
  }

  static _difference(filterA, filterB) {
    let MS = ComplexSet;
    let MMS = MappedComplexSet;
    return [MS.difference(filterA[0], filterB[0]), MMS.difference(filterA[1], filterB[1])];
  }

  static _symmetricDifference(filterA, filterB) {
    let MS = ComplexSet;
    let MMS = MappedComplexSet;
    return [MS.symmetricDifference(filterA[0], filterB[0]), MMS.symmetricDifference(filterA[1], filterB[1])];
  }

} //generalized methods with Filtering by IDs or sheetSpecific Filtering without disturbing the ExcelID data


class FilteredExcelID extends ExcelID {
  //key: `${ID}:${sheetName}:${colName}`, val: Set of blacklisted idxs
  constructor(workbook, sheetsToProcess, IDcolumn) {
    super(workbook, sheetsToProcess, IDcolumn);

    _defineProperty(this, "IDs", void 0);

    _defineProperty(this, "mappedIDSequences", void 0);

    this.clearFilters();
  }

  getIDs() {
    if (!this.IDs.complementMode) return this.IDs;
    let retIDs = new ComplexSet(Object.keys(this.cases));
    retIDs.intersectionUpdate(this.IDs);
    return retIDs;
  }

  setIDs(IDs) {
    this.IDs = new ComplexSet(IDs);
  }

  clearFilters() {
    this.IDs = new ComplexSet(Object.keys(this.cases));
    this.mappedIDSequences = new MappedComplexSet();
  }

  setFilter(filter) {
    this.IDs = filter[0];
    this.mappedIDSequences = filter[1];
  }

  mergeFilter(filter) {
    this.IDs.intersectionUpdate(filter[0]);
    this.mappedIDSequences.unionUpdate(filter[1]);
  }

  getCurrentFilterCopy() {
    return [this.IDs.getCopy(), this.mappedIDSequences.getCopy()];
  }

  getSequenceKey(ID, sheetName) {
    return `${ID}:${sheetName}`;
  }

  addSequenceKey(ID, sheetName, idx) {
    let key = getSequenceKey(ID, sheetName); //initialize mapped ID sequence, and get complement mode

    let complementMode = false;
    if (!this.mappedIDSequences.has(key)) this.mappedIDSequences.set(key, new ComplexSet());else complementMode = this.mappedIDSequences(key).complementMode;
    if (!complementMode) this.mappedIDSequences.get(key).add(idx);else this.mappedIDSequences.get(key).delete(idx);
  }

  getSequence(ID, sheetName, colName) {
    if (typeof this.cases[ID][sheetName][colName] === 'undefined') throw new Error(`Undefined Data while Sequencing @${ID} > ${sheetName} > ${colName}`);
    let sKey = this.getSequenceKey(ID, sheetName);
    let sequence;
    if (typeof this.cases[ID][sheetName][colName] !== 'object') sequence = new ComplexSet([0]);else sequence = new ComplexSet(new Array(this.cases[ID][sheetName][colName].length).keys()); //must always be a noncomplement set

    if (this.mappedIDSequences.has(sKey)) {
      //console.log(sKey, sequence);
      sequence.intersectionUpdate(this.mappedIDSequences.get(sKey)); //will only return a noncomplement set
      //console.log(sKey, sequence);
    }

    return sequence;
  } //data = dataterm = term, should probably rename this at some point


  sequenceData(ID, sheetName, colName, depthParse = true) {
    let ret = [];

    if (!this.cases[ID] || !this.cases[ID][sheetName]) {
      //console.error(`[README] The ID: '${ID}', failed to be sequenced under ${sheetName} > ${colName}`)
      throw new Error(`[README] The ID: '${ID}', failed to be sequenced under ${sheetName} > ${colName}`);
      return ret;
    } //console.log('a',ret, data, sheetName, colName,'|', this.cases[ID][sheetName]);


    let data = this.cases[ID][sheetName][colName];
    if (typeof data === 'undefined') throw new Error(`Undefined Data while Sequencing @${ID} > ${sheetName} > ${colName}`);
    if (typeof data !== 'object') data = [data];
    let sequence = this.getSequence(ID, sheetName, colName);
    sequence.forEach(idx => {
      if (depthParse && typeof data[idx] !== 'undefined') {
        let subdata = data[idx].replace(/\r\n/g, '\n');
        subdata = data[idx].replace(/\r/g, '\n');
        ret.push(...subdata.split('\n'));
      } else ret.push(data[idx]);
    });
    return ret;
  }

  getBasicUniversalFilter(sheetName, colName, searchTerm) {
    let keepIDs = new ComplexSet();
    let mappedIDSequences = new MappedComplexSet();

    for (let ID of this.IDs) {
      let data = this.sequenceData(ID, sheetName, colName, false);
      let sKey = this.getSequenceKey(ID, sheetName, colName);

      for (let i = 0; i < data.length; i++) {
        if (data[i].toLowerCase() == searchTerm.toLowerCase()) {
          keepIDs.add(ID);
          if (!mappedIDSequences.has(sKey)) mappedIDSequences.set(sKey, new ComplexSet());
          mappedIDSequences.get(sKey).add(i);
          continue;
        }
      }
    } //console.log('kept', keepIDs.toStr())


    return [keepIDs, mappedIDSequences];
  }

}

class FilteredExcelWrapper extends FilteredExcelID {
  getN() {
    return this.getIDs().size;
  }

  getDataTerms(sheetName, colName) {
    let ret = {};

    for (let ID of this.getIDs()) {
      let alreadyCounted = new Set(); //to only get unique IDs

      for (let term of this.sequenceData(ID, sheetName, colName)) {
        //console.log(term)
        if (alreadyCounted.has(term)) continue; //skip if term already counted for this ID

        if (!(term in ret)) ret[term] = 0;
        ret[term] += 1;
        alreadyCounted.add(term);
      }

      ; //console.log(ID);
    }

    return ret;
  } //caseinsensitive, exact matches only


  getMatches(sheetName, colName, searchTerm) {
    let ret = [];

    for (let ID of this.getIDs()) {
      for (let term of this.sequenceData(ID, sheetName, colName)) {
        if (term.toLowerCase() == searchTerm.toLowerCase()) {
          ret.push(ID);
          break;
        }
      }
    }

    return ret;
  } //caseinsensitive, exact matches only


  getNumMatches(sheetName, colName, searchTerm) {
    let ret = 0;

    for (let ID of this.getIDs()) {
      for (let term of this.sequenceData(ID, sheetName, colName)) {
        if (term.toLowerCase() == searchTerm.toLowerCase()) {
          ret += 1;
          break;
        }
      }
    }

    return ret;
  } //sample-based statistics


  getOneVarStats(sheetName, colName) {
    let data = [];

    for (let ID of this.getIDs()) {
      for (let term of this.sequenceData(ID, sheetName, colName)) {
        if (!isNaN(term) && term != '') {
          data.push(Number(term)); //console.log(term, Number(term))
        }
      }
    }

    return Statistics.oneVarStats(data);
  }

  getOverlapsWithinDatapoint(sheetName, colName) {
    let ret = {};
    let termIDs = {};

    for (let ID of this.getIDs()) {
      this.sequenceData(ID, sheetName, colName).forEach(term => {
        if (!(term in termIDs)) {
          termIDs[term] = new ComplexSet();
          ret[term] = {};
        } //console.log(term, ID)


        termIDs[term].add(ID);
      });
    } //console.log(termIDs)


    let terms = Object.keys(termIDs);

    for (let i = 0; i < terms.length; i++) {
      for (let j = i + 1; j < terms.length; j++) {
        ret[terms[i]][terms[j]] = ComplexSet.intersection(termIDs[terms[i]], termIDs[terms[j]]).size;
        ret[terms[j]][terms[i]] = ret[terms[i]][terms[j]];
      }
    }

    return ret;
  }

} //generates Vigibase data points


class CustomVigiDataPoints extends FilteredExcelWrapper {
  constructor(workbook, configuration) {
    super(workbook, configuration.Sheets, configuration.UniqueIDidentifer);

    _defineProperty(this, "dataIntegrity", void 0);

    _defineProperty(this, "lastGeneratedDP", null);

    this.dataIntegrity = this.verifyDataIntegrity();

    try {
      if (configuration.generateCustomDataPoints) this.genCustomDataPoints();
    } catch (err) {
      console.error('[README] An error occured generating custom data point:', this.lastGeneratedDP);
      console.error('[README] The following dataPoints were processed before the failure');
      console.error(JSON.stringify(this.getDataPoints(), null, '  '));
      console.error('Passing Error upstream');
      throw err;
    } //for(let ID in this.cases) console.log(this.cases[ID]['Cases']['Custom|Continent of primary source']);

  }

  daysBetween(startDateString, endDateString) {
    //only having the year is too vague, at least both strings need at least the month
    if (startDateString.length <= 4 || endDateString.length <= 4) return undefined;
    return Math.round((Date.parse(endDateString) - Date.parse(startDateString)) / 86400000);
  }

  verifyDataIntegrity() {
    console.log("[INFO] Checking Data Consistency");
    let ret = true;
    let delIDs = [];

    for (let ID in this.cases) {
      for (let sheetName of this.sheetsToProcess) {
        if (!this.cases[ID][sheetName]) {
          delIDs.push(ID);
          ret = false;
          break;
        }
      }
    }

    if (delIDs.length > 0) {
      console.error('[README] Data Integrity Checks Failed for the following IDs');
      console.error('[README]', delIDs);
      console.error('[README] These IDs will be deleted from the processed dataset');
    }

    for (let ID of delIDs) {
      delete this.cases[ID];
      this.IDs.delete(ID);
    }

    return ret;
  }

  genCustomDataPoints() {
    //ignore filters and force for all datapoints
    console.log("[INFO] Processing Custom Datapoints");
    this.iterableKeys['Custom'] = {};
    this.genCustomVigibaseInitialYear();
    this.genCustomContinentOfPrimarySource();
    this.genCustomReactionsDuration();
    this.genCustomDrugRoleCount();
    this.genCustomDrugDosePerDay();
  } //this function just helps deal with bad data, in an ideal world it would be unnecessary


  getFirstDatum(val) {
    //console.log(val);
    if (typeof val === 'object') {
      return val[0];
    } //console.log(val)


    return val;
  }

  genCustomVigibaseInitialYear() {
    this.lastGeneratedDP = 'genCustomVigibaseInitialYear';
    this.iterableKeys['Cases']['Custom|VigiBase initial year'] = false;

    for (let ID in this.cases) {
      //console.log(ID);
      let year = this.getFirstDatum(this.cases[ID]['Cases']['VigiBase initial date']).substring(0, 4);
      this.cases[ID]['Cases']['Custom|VigiBase initial year'] = year;
    }
  }

  genCustomContinentOfPrimarySource() {
    this.lastGeneratedDP = 'genCustomContinentOfPrimarySource';
    this.iterableKeys['Cases']['Custom|Continent of primary source'] = false;
    let Americas = ['Canada', 'United States of America', 'Argentina', 'Brazil', 'Mexico', 'Colombia', 'Argentina', 'Canada', 'Peru', 'Venezuela', 'Chile', 'Ecuador', 'Guatemala', 'Cuba', 'Bolivia', 'Haiti', 'Dominican Republic', 'Honduras', 'Paraguay', 'Nicaragua', 'El Salvador', 'Costa Rica', 'Panama', 'Puerto Rico', 'Uruguay', 'Jamaica', 'Trinidad and Tobago', 'Guyana', 'Suriname', 'Bahamas', 'Belize', 'Barbados', 'Saint Lucia', 'Saint Vincent and the Grenadines', 'United States Virgin Islands', 'Grenada', 'Antigua and Barbuda', 'Dominica', 'Bermuda', 'Cayman Islands', 'Greenland', 'Saint Kitts and Nevis', 'Sint Maarten', 'Turks and Caicos Islands', 'Saint Martin', 'British Virgin Islands', 'Caribbean Netherlands', 'Anguilla', 'Saint Barthélemy', 'Saint Pierre and Miquelon', 'Montserrat', 'Falkland Islands'];
    let Europe = ['Austria', 'Belgium', 'Czechia', 'France', 'Germany', 'Greece', 'Italy', 'Norway', 'Poland', 'Slovakia', 'Spain', 'Switzerland', 'United Kingdom of Great Britain and Northern Ireland', 'Bulgaria', 'Denmark', 'Estonia', 'Russia', 'Germany', 'United Kingdom', 'France', 'Italy', 'Spain', 'Ukraine', 'Poland', 'Romania', 'Netherlands', 'Belgium', 'Czech Republic', 'Greece', 'Portugal', 'Sweden', 'Hungary', 'Belarus', 'Austria', 'Serbia', 'Switzerland', 'Bulgaria', 'Denmark', 'Finland', 'Slovakia', 'Norway', 'Ireland', 'Croatia', 'Moldova', 'Bosnia and Herzegovina', 'Albania', 'Lithuania', 'North Macedonia', 'Slovenia', 'Latvia', 'Estonia', 'Montenegro', 'Luxembourg', 'Malta', 'Iceland', 'Andorra', 'Monaco', 'Liechtenstein', 'San Marino', 'Holy See', 'New Zealand'];
    let Australia = ['Australia'];
    let Asia = ['Japan', 'Korea (the Republic of)', 'China', 'India', 'Indonesia', 'Pakistan', 'Bangladesh', 'Japan', 'Philippines', 'Vietnam', 'Turkey', 'Iran', 'Thailand', 'Myanmar', 'South Korea', 'Iraq', 'Afghanistan', 'Saudi Arabia', 'Uzbekistan', 'Malaysia', 'Yemen', 'Nepal', 'North Korea', 'Sri Lanka', 'Kazakhstan', 'Syria', 'Cambodia', 'Jordan', 'Azerbaijan', 'United Arab Emirates', 'Tajikistan', 'Israel', 'Laos', 'Lebanon', 'Kyrgyzstan', 'Turkmenistan', 'Singapore', 'Oman', 'State of Palestine', 'Kuwait', 'Georgia', 'Mongolia', 'Armenia', 'Qatar', 'Bahrain', 'Timor-Leste', 'Cyprus', 'Bhutan', 'Maldives', 'Brunei', 'Taiwan', 'Hong Kong', 'Macao'];
    let Africa = ['Nigeria', 'Ethiopia', 'Egypt', 'DR Congo', 'Tanzania', 'South Africa', 'Kenya', 'Uganda', 'Algeria', 'Sudan', 'Morocco', 'Angola', 'Mozambique', 'Ghana', 'Madagascar', 'Cameroon', 'Côte d\'Ivoire', 'Niger', 'Burkina Faso', 'Mali', 'Malawi', 'Zambia', 'Senegal', 'Chad', 'Somalia', 'Zimbabwe', 'Guinea', 'Rwanda', 'Benin', 'Burundi', 'Tunisia', 'South Sudan', 'Togo', 'Sierra Leone', 'Libya', 'Congo', 'Liberia', 'Central African Republic', 'Mauritania', 'Eritrea', 'Namibia', 'Gambia', 'Botswana', 'Gabon', 'Lesotho', 'Guinea-Bissau', 'Equatorial Guinea', 'Mauritius', 'Eswatini', 'Djibouti', 'Comoros', 'Cabo Verde', 'Sao Tome & Principe', 'Seychelles'];
    let Antartica = ['Antartica'];

    for (let ID in this.cases) {
      let country = this.getFirstDatum(this.cases[ID]['Cases']['Country of primary source']);
      let continent = `Unknown | ${country}`;
      if (Americas.includes(country)) continent = 'Americas';else if (Europe.includes(country)) continent = 'Europe';else if (Asia.includes(country)) continent = 'Asia';else if (Africa.includes(country)) continent = 'Africa';else if (Australia.includes(country)) continent = 'Australia';else if (Antartica.includes(country)) continent = 'Antartica';
      this.cases[ID]['Cases']['Custom|Continent of primary source'] = continent;
    } //for(let ID in this.cases) console.log(this.cases[ID]['Cases']['Custom|Continent of primary source']);

  }

  genCustomReactionsDuration() {
    this.lastGeneratedDP = 'genCustomReactionsDuration';
    this.iterableKeys['Reactions']['Custom|Duration (days)'] = true;

    for (let ID in this.cases) {
      this.cases[ID]['Reactions']['Custom|Duration (days)'] = []; //console.log(this.cases[ID]['Reactions']['Start date']);

      let startDates = this.cases[ID]['Reactions']['Start date'];
      let endDates = this.cases[ID]['Reactions']['End date'];

      for (let i = 0; i < startDates.length; i++) {
        this.cases[ID]['Reactions']['Custom|Duration (days)'].push(String(this.daysBetween(startDates[i], endDates[i]))); //if(startDates[i].length > 1) console.log('SD', startDates[i], endDates[i])
      }
    }
  }

  genCustomDrugRoleCount() {
    this.lastGeneratedDP = 'genCustomDrugRoleCount';
    this.iterableKeys['Drugs']['Custom|Total Suspect'] = false;
    this.iterableKeys['Drugs']['Custom|Total Concomitant'] = false;
    this.iterableKeys['Drugs']['Custom|Total Interacting'] = false;

    for (let ID in this.cases) {
      this.cases[ID]['Drugs']['Custom|Total Suspect'] = 0;
      this.cases[ID]['Drugs']['Custom|Total Concomitant'] = 0;
      this.cases[ID]['Drugs']['Custom|Total Interacting'] = 0;

      for (let role of this.cases[ID]['Drugs']['Role']) {
        if (role === 'Suspect') this.cases[ID]['Drugs']['Custom|Total Suspect']++;else if (role === 'Concomitant') this.cases[ID]['Drugs']['Custom|Total Concomitant']++;else if (role === 'Interacting') this.cases[ID]['Drugs']['Custom|Total Interacting']++;
      }

      this.cases[ID]['Drugs']['Custom|Total Suspect'] = String(this.cases[ID]['Drugs']['Custom|Total Suspect']);
      this.cases[ID]['Drugs']['Custom|Total Concomitant'] = String(this.cases[ID]['Drugs']['Custom|Total Concomitant']);
      this.cases[ID]['Drugs']['Custom|Total Interacting'] = String(this.cases[ID]['Drugs']['Custom|Total Interacting']);
    }
  }

  genCustomDrugDosePerDay() {
    this.lastGeneratedDP = 'genCustomDrugDosePerDay';
    let tmpterms = new Set();
    this.iterableKeys['Drugs']['Custom|Dose (per Day)'] = true;

    for (let ID in this.cases) {
      this.cases[ID]['Drugs']['Custom|Dose (per Day)'] = [];

      for (let i = 0; i < this.cases[ID]['Drugs']['Dose'].length; i++) {
        let dailyDose = this.cases[ID]['Drugs']['Dose'][i];
        let doseFreq = this.cases[ID]['Drugs']['Dosage regimen'][i];

        if (dailyDose === '') {
          if (doseFreq === '') {
            //console.log('skip');
            this.cases[ID]['Drugs']['Custom|Dose (per Day)'].push('');
            continue;
          }

          dailyDose = '1';
        }

        dailyDose = Number(dailyDose);
        doseFreq = doseFreq.toLowerCase().replace(/‒/g, '-').replace('hours', 'hour').replace('days', 'day').replace('weeks', 'week').replace('months', 'month').replace('years', 'year').replace('- -', '1 day').replace('-', '1');

        if (doseFreq === '' || doseFreq === '1 per 1 day') {
          this.cases[ID]['Drugs']['Custom|Dose (per Day)'].push(String(dailyDose));
          continue;
        }

        doseFreq = doseFreq.split(' ');
        if (doseFreq.includes('hour')) dailyDose = dailyDose * 24;else if (doseFreq.includes('week')) dailyDose = dailyDose / 7;else if (doseFreq.includes('month')) dailyDose = dailyDose / 30;else if (doseFreq.includes('year')) dailyDose = dailyDose / 365;

        if (doseFreq.includes('cyclical') || doseFreq.includes('total') || doseFreq.includes('necessary')) {
          this.cases[ID]['Drugs']['Custom|Dose (per Day)'].push('');
          continue;
        }

        tmpterms.add(doseFreq[doseFreq.length - 1]);

        if (doseFreq.length === 4) {
          dailyDose = dailyDose * Number(doseFreq[0]);
          dailyDose = dailyDose / Number(doseFreq[2]);
        }

        this.cases[ID]['Drugs']['Custom|Dose (per Day)'].push(String(dailyDose)); //console.log(dailyDose.toFixed(2), '\t', this.cases[ID]['Drugs']['Dose'][i], '\t', this.cases[ID]['Drugs']['Dosage regimen'][i])
      }
    } //console.log(tmpterms);

  }

} //internal Vigibase specific methods


class VigiMethods extends CustomVigiDataPoints {
  constructor(workbook, configuration) {
    super(workbook, configuration);
  }

  getMaxDose(ID, drugColumn) {
    let sequence = this.getSequence(ID, 'Drugs', drugColumn);
    let maxDoseIdx = -1;
    let maxDose = "";
    sequence.forEach(idx => {
      //console.log(maxDoseIdx, maxDose, idx, this.cases[ID]['Drugs']['Custom|Dose (per Day)'][idx])
      if (Number(this.cases[ID]['Drugs']['Custom|Dose (per Day)'][idx]) > Number(maxDose)) {
        maxDoseIdx = idx;
        maxDose = Number(this.cases[ID]['Drugs']['Custom|Dose (per Day)'][idx]);
      }
    });
    if (!maxDose) maxDose = "Unknown"; //if(maxDose === "100") console.log(900, ID);
    //if(maxDose === "900") console.log(900, ID);

    return [maxDoseIdx, maxDose];
  }

  getFirstReaction(ID, afterDate = "") {
    if (!afterDate) return [-1, -1];
    let sequence = this.getSequence(ID, 'Reactions', 'Start date');
    let reactionIdx = -1;
    let currDelt = -1;
    sequence.forEach(idx => {
      let delt = this.daysBetween(afterDate, this.cases[ID]['Reactions']['Start date'][idx]);

      if (delt > 0) {
        if (currDelt === -1 || delt < currDelt) {
          currDelt = delt;
          reactionIdx = idx;
        }
      }
    });
    return [reactionIdx, currDelt];
  }

}

class VigiWrapper extends VigiMethods {
  getDrugStats(drugColumn, term, includeDaysListStr) {
    let daysList = true;
    if ("Exclude Days List" === includeDaysListStr) daysList = false;
    let ret = {};
    let originalFilter = this.getCurrentFilterCopy();
    let universalDrugFilter = this.getBasicUniversalFilter('Drugs', drugColumn, term);
    let crossFilter = AdvancedFilterLogic.intersection(originalFilter, universalDrugFilter); //union subsequence

    crossFilter = AdvancedFilterLogic.normalizeIDs(crossFilter, this.cases, this.IDcolumn);
    this.setFilter(crossFilter);
    ret[`Total IDs with ${term}`] = this.getN();
    ret[`Total IDs with only ${term}`] = 0;
    ret[`Total IDs with ${term} + 1 other drug`] = 0;
    ret[`Total IDs with ${term} + >=2 other drugs`] = 0;

    for (let ID of this.getIDs()) {
      count = this.sequenceData(ID, 'Drugs', drugColumn, term, false).length;
      if (count === 0) throw new Error('Something went wrong getting drug stat counts');else if (count === 1) ret[`Total IDs with only ${term}`] += 1;else if (count === 2) ret[`Total IDs with ${term} + 1 other drug`] += 1;else if (count > 2) ret[`Total IDs with ${term} + >=2 other drugs`] += 1;
    }

    crossFilter = AdvancedFilterLogic._intersection(originalFilter, universalDrugFilter); //intersection subsequence

    crossFilter = AdvancedFilterLogic.normalizeIDs(crossFilter, this.cases, this.IDcolumn);
    this.setFilter(crossFilter);
    ret[`Max Dose of Drug (per Day)`] = {};
    ret[`First Reaction after Max Dose`] = {};
    ret[`First Reaction after Max Dose`]['Total Available Unique IDs'] = 0;
    ret[`First Reaction after Max Dose`]['Outcomes'] = {};
    ret[`First Reaction after Max Dose`]['Days from Max Dose to Reaction One Var Stats'] = {};
    ret[`First Reaction after Max Dose`]['Days from Max Dose to Reaction'] = [];

    for (let ID of this.getIDs()) {
      let [maxDoseIdx, maxDose] = this.getMaxDose(ID, drugColumn);
      if (maxDose !== "Unknown") maxDose = String(maxDose) + " " + this.cases[ID]['Drugs']['Dose unit'][maxDoseIdx];
      if (!ret[`Max Dose of Drug (per Day)`][maxDose]) ret[`Max Dose of Drug (per Day)`][maxDose] = 0;
      ret[`Max Dose of Drug (per Day)`][maxDose] += 1;
      let [rIdx, rDays] = this.getFirstReaction(ID, this.cases[ID]['Drugs']['Start date'][maxDoseIdx]);

      if (rIdx !== -1) {
        ret[`First Reaction after Max Dose`]['Total Available Unique IDs'] += 1;
        let outcome = this.cases[ID]['Reactions']['Outcome'][rIdx];
        if (!ret[`First Reaction after Max Dose`]['Outcomes'][outcome]) ret[`First Reaction after Max Dose`]['Outcomes'][outcome] = 0;
        ret[`First Reaction after Max Dose`]['Outcomes'][outcome] += 1;
        ret[`First Reaction after Max Dose`]['Days from Max Dose to Reaction'].push(rDays);
      }
    }

    if (ret[`First Reaction after Max Dose`]['Total Available Unique IDs'] > 2) ret[`First Reaction after Max Dose`]['Days from Max Dose to Reaction One Var Stats'] = Statistics.oneVarStats(ret[`First Reaction after Max Dose`]['Days from Max Dose to Reaction']);
    if (!daysList) delete ret[`First Reaction after Max Dose`]['Days from Max Dose to Reaction'];
    return ret;
  }

}
//let eD=null
onmessage = function (event) {
  console.log('Message Receieved', event); //vigiLoadWorkbook(event)

  console.log('Posting Message');
  postMessage('messagePost');
};

function vigiLoadWorkbook(event) {
  showLoadingModal();
  event.stopPropagation();
  event.preventDefault();
  if (!event || !event.target || !event.target.files || event.target.files.length === 0) return;
  const filename = event.target.files[0].name; //document.getElementById('vigiLoadWorkbook_label').textArea

  document.getElementById('vigiLoadWorkbook_label').innerHTML = filename; //readSingleFile(event);

  try {
    readExcelFile(event);
  } catch (err) {
    handleError(err);
  }
}

function readExcelFile(event) {
  let file = event.target.files[0];
  if (!file) return;
  let reader = new FileReader();

  reader.onloadstart = function (anEvent) {};

  reader.onload = function (anEvent) {
    try {
      var data = new Uint8Array(anEvent.target.result);
      var workbook = XLSX.read(data, {
        type: 'array'
      });
      eD = new VigiWrapper(workbook, ['Cases', 'Drugs', 'Reactions'], 'UMC report ID');
      hideLoadingModal();
      testExcelFile();
    } catch (err) {
      handleError(err);
    }
  };
}

function readSingleFile(event) {
  let file = event.target.files[0];
  if (!file) return;
  let reader = new FileReader();

  reader.onload = function (anEvent) {
    var contents = anEvent.target.result;
    console.log(contents);
  };

  reader.readAsText(file);
}
//let eD;
function removeComment(line) {
  commentStartLoc = line.indexOf('//');
  if (commentStartLoc === -1) return line;
  return line.substring(0, commentStartLoc).trim();
}

function getFilter(input, filters) {
  let AFL = AdvancedFilterLogic;
  if (typeof input === 'object') return AFL.normalizeIDs(eD.getBasicUniversalFilter(...input), eD.cases, eD.IDcolumn);
  ;
  input = input.replace(/,/g, ' , ');
  input = input.replace(/\(/g, '( ');
  input = input.replace(/\)/g, ' )');

  for (let property in filters) {
    let re = new RegExp(` ${property} `, "g");
    input = input.replace(re, `filters['${property}']`);
  }

  input = input.replace(/A\(/g, 'AFL.complement(');
  input = input.replace(/_U\(/g, 'AFL._union(');
  input = input.replace(/_I\(/g, 'AFL._intersection(');
  input = input.replace(/_D\(/g, 'AFL._difference(');
  input = input.replace(/_S\(/g, 'AFL._symmetricDifference(');
  input = input.replace(/U\(/g, 'AFL.union(');
  input = input.replace(/I\(/g, 'AFL.intersection(');
  input = input.replace(/D\(/g, 'AFL.difference(');
  input = input.replace(/S\(/g, 'AFL.symmetricDifference('); //console.log(input)
  //let Universal = TODO 

  return AFL.normalizeIDs(eval(input), eD.cases, eD.IDcolumn); //
} //Input Processing


function matchClosingBracket(str, startIndex) {
  //console.log(str.charAt(startIndex));
  count = 1;
  idx = startIndex;

  while (count > 0) {
    idx++;
    if (str.charAt(idx) === '{') count++;else if (str.charAt(idx) === '}') count--;
  } //console.log(str.charAt(idx), idx-startIndex);


  return idx;
}

function getFilters(input) {
  aFi = input.indexOf('AvailableFilters');

  if (aFi === -1) {
    handleError(new Error('AvailableFilters not found'));
    console.error('Will attempt to continue with no filters pre-processed');
    return null;
  }

  let filters = input.substring(aFi);
  filters = input.substring(input.indexOf('{'));
  filters = filters.substring(0, matchClosingBracket(filters, 0) + 1);
  filters = filters.replace(/\n/g, ' ');
  let pfilters;

  try {
    pfilters = JSON.parse(filters);
  } catch (err) {
    console.error('AvailableFilters is not in proper JSON format, the following failed to parse:');
    console.error(filters);
    console.error('Refer to https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/JSON_bad_parse');
    handleError(err);
    return null;
  }

  filters = {};

  for (let property in pfilters) {
    //console.log(property);
    filters[property] = getFilter(pfilters[property], filters);
    nID = filters[property][0].size;
    nID_m = filters[property][0].complementMode;
    nSequenced = filters[property][1].size;
    console.log("[INFO]", `Processed Filter ${property}: Total IDs ${nID} (${nID_m}), Total Specially Sequenced IDs ${nSequenced}`); //console.log(filters[property])
  }

  return filters;
}

function processCommand(filters, command) {
  let originalCommand = command;

  if (command.indexOf("'") !== -1) {
    console.error(`[README] Warning the command: '${command}' contains single quotes (') instead of double quotes ("), attempting to continue`);
  }

  console.log("[INFO] Processing command:", command);
  let currFilter = undefined;
  let filterName = undefined;

  if (command.indexOf(':') > 0 && command.indexOf(':') < command.indexOf(' ')) {
    currFilter = filters[command.split(':')[0]];
    filterName = command.split(':')[0];
    command = command.substring(command.indexOf(':') + 1);
  }

  command = command.trim();
  command = 'eD.' + command.replace(' ', '(');
  if (command.indexOf('(') === -1) command += '(';
  command += ')';
  command = command.replace(/\" \"/g, '", "'); //console.log(command);

  if (currFilter) eD.setFilter(currFilter);
  let n = eD.getN();
  if (command.indexOf('eD.get') === 0) ret = eval(command);else if (command.indexOf('eD.plot') === 0) {
    command = 'PlotCreator' + command.substring(2);
    if (filterName) command = command.slice(0, -1) + `, filterName = "${filterName}")`; //console.log(command);

    eval(command);
    ret = 'See Plots';
  } else {
    console.error(`[README] Failed to recognize command: '${originalCommand}', it was transcompiled to ${command}`);
    ret = `ERROR: ${command}`;
  }
  if (currFilter) eD.clearFilters();
  return [command, filterName, n, ret];
}

function processCommands(filters, input, prettyPrint = false) {
  let aFI = input.indexOf('AvailableFilters');

  if (aFI !== -1) {
    input = input.substring(aFI);
    input = input.substring(input.indexOf('{'));
    input = input.substring(matchClosingBracket(input, 0) + 1).trim();
  }

  let output = "";
  pM.clearPlots();

  for (let command of input.split('\n')) {
    if (command.trim() !== '') {
      let [commandline, currFilter, n, ret] = processCommand(filters, command);
      output += `Commandline: ${commandline}\n`;
      if (currFilter) output += `  Filter Name: ${currFilter}\n`;
      output += `  # of Processed IDs: ${n}\n`;
      if (prettyPrint) output += `  JSON Command Output: ${JSON.stringify(ret, null, '    ')}\n\n`;else output += ` JSON Command Output: ${JSON.stringify(ret)}\n\n`;
    }
  }

  pM.refreshPlots();
  return output;
}

function removeComments(raw_input) {
  let input = ''; //strip comments

  for (let i = 0; i < raw_input.length; i++) {
    line = removeComment(raw_input[i]);

    if (line !== '') {
      input += line;
      input += '\n'; //console.log(line);
    }
  }

  return input;
}

function processInput(prettyPrint = false, timeoutForModal = true) {
  if (timeoutForModal) {
    showProcessingModal(); //console.log('b', timeoutForModal);

    setTimeout(function () {
      processInput(prettyPrint = prettyPrint, timeoutForModal = false);
    }, 500);
    return;
  }

  try {
    let raw_input = document.getElementById('inputOnPage').value.split('\n');
    let input = removeComments(raw_input); //get filters

    let lT;
    lT = 0 - performance.now();
    filters = getFilters(input);
    lT += performance.now();
    console.log('[INFO][PERF] Filters precalculated in', lT / 1000, 's'); //run commands

    lT = 0 - performance.now();
    output = processCommands(filters, input, prettyPrint = prettyPrint);
    lT += performance.now();
    document.getElementById('outputOnPage').value = output;
    console.log('[INFO][PERF] Commands processed in', lT / 1000, 's');
    hideProcessingModal(); //create plots
  } catch (err) {
    handleError(err);
    let pMF = document.getElementById('processingModal_footer');
    pMF.innerHTML = '<p><b>A Critical unexpected exception occured. Please press force close and check the Progress Log.</b></p><button type="button" data-dismiss="modal" data-target="#processingModal" class="btn btn-warning">Force Close</button>';
  }
}
var eD = null;
let eD_configuration = {
  "Sheets": ["Cases", "Drugs", "Reactions"],
  "UniqueIDidentifer": "UMC report ID",
  "generateCustomDataPoints": true
};
/**Another Old Configuration
{
  "Sheets": [
    "CaseInfo"
  ],
  "UniqueIDidentifer": "ReportID",
  "generateCustomDataPoints": false
}
**/
//edit Configuration

function editConfiguration() {
  document.getElementById('editConfigModal_textarea').value = JSON.stringify(eD_configuration, null, '  ');
  $('#editConfigModal').modal('show');
}

function saveConfiguration() {
  try {
    eD_configuration = JSON.parse(document.getElementById('editConfigModal_textarea').value);
  } catch (err) {
    handleError(err);
  }

  console.log('[INFO] Configuration Updated: ', eD_configuration);
  $('#editConfigModal').modal('show');
} //Loading File


let LoadFile = function LoadFile(opts) {
  let undefinedFunc = function () {};

  function handleFile(event) {
    event.stopPropagation();
    event.preventDefault();
    let file = event.target.files[0]; //console.log('Available file properties:')
    //for (let property in file) console.log('- ',property);

    function vigiLoadWorkbook(event) {
      //showLoadingFileModal();
      if (!event || !event.target || !event.target.files || event.target.files.length === 0) return;
      const filename = event.target.files[0].name; //document.getElementById('vigiLoadWorkbook_label').textArea

      document.getElementById('vigiLoadWorkbook_label').innerHTML = filename; //readSingleFile(event);

      try {
        readExcelFile(event);
      } catch (err) {
        handleError(err);
      }
    }

    if (file.size > 2e5) {
      let estimatedMaxLoadTime = Math.ceil(file.size / 4e5);
      let message = `${file.name} is a large file (${file.size} Bytes)`;
      message += '\nThis page may freeze while the file is being read into memory.';
      message += '\nEstimated Max Load Time: ' + estimatedMaxLoadTime + ' seconds.';
      message += '\nEstimated Max Processing Time: ' + Math.ceil(estimatedMaxLoadTime / 10) + ' second(s).';
      message += '\nPress OK to start.';
      showLoadingFileModal(file.name);
      alert(message);
      if (opts.preload) opts.preload;
    }

    setTimeout(function () {
      vigiLoadWorkbook(event);
    }, 1);
  } //reads into eD


  function readExcelFile(event) {
    let file = event.target.files[0];
    if (!file) return;
    let reader = new FileReader();

    reader.onloadstart = function (anEvent) {};

    reader.onload = function (anEvent) {
      try {
        let loadFileTime;
        loadFileTime = 0 - performance.now();
        let data = new Uint8Array(anEvent.target.result);
        let workbook = XLSX.read(data, {
          type: 'array'
        });
        data = null;
        loadFileTime += performance.now();
        console.log('[INFO][PERF] Workbook Loaded in', loadFileTime / 1000, 's');
        loadFileTime = 0 - performance.now();
        eD = new VigiWrapper(workbook, eD_configuration);
        workbook = null;
        loadFileTime += performance.now();
        console.log('[INFO][PERF] Processed Data in', loadFileTime / 1000, 's'); //Enable Input Buttons

        $('#notifyLoadSuccess').toast({
          delay: 10000
        });
        $('#notifyLoadSuccess').toast('show');
        enableInputButtons(); //Report Data Integrity

        if (!eD.dataIntegrity) {
          //onsole.log('asdf', eD.dataIntegrity, eD.lastGeneratedDP);
          throw new Error('Some IDs failed to pass data integrity checks and were deleted (see above).  This is a nonfatal error, all other IDs passed initial validation. Input buttons have been enabled, you may continue.');
        } //Update Loading Modal, and attempt to hide it


        let lMF = document.getElementById('loadingFileModal_footer');
        lMF.innerHTML = '<p>Loading successfully completed, but this dialog failed to close. Please press force close to continue.</p><button type="button" data-dismiss="modal" data-target="#loadingFileModal" class="btn btn-success">Force Close</button>';
        setTimeout(function () {
          hideLoadingFileModal();
        }, 1); //lMF.innerHTML += 'Loading complete, this dialog failed to close, press force close to continue.'
      } catch (err) {
        let lMF = document.getElementById('loadingFileModal_footer');
        lMF.innerHTML = '<p>An Exception occured. Please press force close and check the Progress Log.</p><button type="button" data-dismiss="modal" data-target="#loadingFileModal" class="btn btn-warning">Force Close</button>';
        disableLoadingFileModalSpinner();
        handleError(err);
      }
    };

    reader.readAsArrayBuffer(file);
  }

  if (opts.inputElement) {
    opts.inputElement.addEventListener('change', handleFile, false);
  }

  if (opts.dropElement) {
    opts.dropElement.addEventListener('drop', handleFile, false);
  }
}; //NON Specific Element Events


function enableInputButtons() {
  document.getElementById('processCommandsBtn').removeAttribute("disabled");
  document.getElementById('processCommandsBtn_pretty').removeAttribute("disabled"); //document.getElementById('generateOverviewPlots').removeAttribute("disabled");

  document.getElementById('smallScreenPlotSwitch').removeAttribute("disabled");
  document.getElementById('createCommandHelper_btn').removeAttribute("disabled");
  document.getElementById('createFilterHelper_btn').removeAttribute("disabled");
}

function copyToClipboard(ID, isTextArea = false) {
  let element = document.getElementById(ID);
  let toCopy;
  if (isTextArea) toCopy = element.value;else toCopy = element.innerHTML;
  toCopy = toCopy.replace(/<\/span>/g, '');
  toCopy = toCopy.replace(/<br>/g, '\n');
  toCopy = toCopy.replace(/<br \/>/g, '\n');
  toCopy = toCopy.replace(/<span class="bg-danger">/g, '');
  navigator.clipboard.writeText(toCopy); //console.log("Copied", ID)
} //Plot Stuff


function plotSwitchUpdate() {
  if (document.getElementById('smallScreenPlotSwitch').checked) {
    pM.setMaxPerRow(1);
  } else {
    pM.setMaxPerRow(2);
  }

  ;
} //Processing Modal


function enableProcessingModalSpinner() {
  document.getElementById('processingModal_spinner').innerHTML = '<div class="spinner-grow text-primary" ></div>';
}

function disableProcessingModalSpinner() {
  document.getElementById('processingModal_spinner').innerHTML = '';
}

function showProcessingModal() {
  let mTitle = document.getElementById('processingModal_title');
  mTitle.innerHTML = 'Processing Filters & Commands';
  enableProcessingModalSpinner();
  document.getElementById('processingModal_footer').innerHTML = '<button type="button" data-dismiss="modal" data-target="#processingModal" class="btn btn-danger">Force Close</button>';
  $('#processingModal').modal('show');
}

function hideProcessingModal() {
  disableProcessingModalSpinner();
  document.getElementById('processingModal_footer').innerHTML = '<b><p>Processing successfully completed, but this dialog failed to close. Please press force close to continue.</p></b><button type="button" data-dismiss="modal" data-target="#processingModal" class="btn btn-success">Force Close</button>';
  $('#processingModal').modal('hide');
} //Loading Modal


function enableLoadingFileModalSpinner() {
  document.getElementById('loadingFileModal_spinner').innerHTML = '<div class="spinner-grow text-primary" ></div>';
}

function disableLoadingFileModalSpinner() {
  document.getElementById('loadingFileModal_spinner').innerHTML = '';
}

function showLoadingFileModal(filename) {
  let mTitle = document.getElementById('loadingFileModal_title');
  mTitle.innerHTML = 'Loading Large File';
  if (filename) mTitle.innerHTML += `: ${filename}`;
  enableLoadingFileModalSpinner();
  document.getElementById('loadingFileModal_footer').innerHTML = '<button type="button" data-dismiss="modal" data-target="#loadingFileModal" class="btn btn-danger">Force Close</button>';
  $('#loadingFileModal').modal('show');
}

function hideLoadingFileModal() {
  disableLoadingFileModalSpinner();
  $('#loadingFileModal').modal('hide');
} //Input/Output Specific-Stuff


function deleteElement(elementID) {
  let e = document.getElementById(elementID);
  e.parentNode.removeChild(e);
}

function splitIOSwitchUpdate() {
  if (document.getElementById('smallScreenIOSwitch').checked) {
    disableSplitIO();
    document.getElementById('smallScreenIOSwitch').checked = true;
  } else {
    enableSplitIO();
  }

  ;
}

function clearTextArea(ID) {
  document.getElementById(ID).value = '';
}

let inputTemplates = {
  "Overview": '//Template: Overview\n//The following template provides an overview of the data\n//Consult the "Filter Showcase" template to see the simplicity & power of filters"\nAvailableFilters = {\n  "suspect_drugs" : [ "Drugs", "Role", "Suspect" ]\n}\n\n//Commands to generate output\ngetDataTerms "Cases" "Country of primary source"\ngetDataTerms "Cases" "Custom|Continent of primary source"\ngetDataTerms "Cases" "Sex"\ngetNumMatches "Cases" "Country of primary source" "United States of America"\ngetOneVarStats "Cases" "Age"\n\n//Commands to generate Plots\nplotPieChart "Cases" "Country of primary source"\nplotBarChart "Cases" "Custom|Continent of primary source"\nplotDoughnotChart "Cases" "Sex"\nplotPieChart "Drugs" "WHODrug active ingredient"\nsuspect_drugs: plotPieChart "Drugs" "WHODrug active ingredient"\n\ngetDataPoints //dumps all usable datapoints at the end of the output',
  "Filter Showcase": '//Template: Filter Showcase\n//The following template showcases how to create and apply basic & advanced filters\n\nAvailableFilters = {\n  //Basic Filters\n  //filter_name: ["Sheet", "Column", "Search Term"]\n  "males" : [ "Cases" , "Sex", "Male" ],\n  "american_patients" : [ "Cases", "Country of primary source", "United States of America" ],\n\n  //Advanced Filters\n  //TODO: Enable Universal Set\n  //I = Intersection, U = Union, D = Difference, S = Symmetric Difference\n  "american_males" : "I(males, american_patients)",\n  "non_american_males" : "D(males, american_patients)",\n}\n\n\n//Commands to generate output\ngetDataTerms "Cases" "Country of primary source"\namerican_patients: getDataTerms "Cases" "Country of primary source"\n\ngetDataTerms "Cases" "Custom|Continent of primary source"\nnon_american_males: getDataTerms "Cases" "Custom|Continent of primary source"\n\ngetDataTerms "Cases" "Sex"\namerican_males: getDataTerms "Cases" "Sex"\n\ngetNumMatches "Cases" "Country of primary source" "United States of America"\nmales: getNumMatches "Cases" "Country of primary source" "United States of America"\n\ngetOneVarStats "Cases" "Age"\nmales: getOneVarStats "Cases" "Age"\n\n//Commands to generate Plots\nplotPieChart "Cases" "Country of primary source"\nmales: plotPieChart "Cases" "Country of primary source"\namerican_patients: plotPieChart "Cases" "Country of primary source"\namerican_males: plotPieChart "Cases" "Country of primary source"\nnon_american_males: plotPieChart "Cases" "Country of primary source"\n\nplotBarChart "Cases" "Custom|Continent of primary source"\nmales: plotBarChart "Cases" "Custom|Continent of primary source"\namerican_patients: plotBarChart "Cases" "Custom|Continent of primary source"\namerican_males: plotBarChart "Cases" "Custom|Continent of primary source"\nnon_american_males: plotBarChart "Cases" "Custom|Continent of primary source"\n\nplotDoughnotChart "Cases" "Sex"\nmales: plotDoughnotChart "Cases" "Sex"\namerican_patients: plotDoughnotChart "Cases" "Sex"\namerican_males: plotDoughnotChart "Cases" "Sex"\nnon_american_males: plotDoughnotChart "Cases" "Sex"\ngetDataPoints //dumps all usable datapoints at the end of the output',
  "Clean Start": 'AvailableFilters = {}'
};

function initInputTemplate() {
  setTemplate("Overview");
  let iTD = document.getElementById('templateDropDown');
  iTD.innerHTML = "";

  for (let property in inputTemplates) {
    iTD.innerHTML += `<div class="dropdown-item" id="templateDropdownElement" onclick="setTemplate('${property}')">${property}</div>`;
  }
}

function setTemplate(propertyName) {
  document.getElementById('useTemplateButton').innerHTML = `Template: ${propertyName}`;
  document.getElementById('inputOnPage').value = inputTemplates[propertyName];
} //Console/Progress Log Stuff


function consoleOnPage_clear() {
  let pageConsole = document.getElementById('consoleOnPage');
  pageConsole.innerHTML = '';
  initPageConsole();
}

function initPageConsole() {
  let pageConsole = document.getElementById('consoleOnPage');
  pageConsole.innerHTML = '';
  console.log("Initiated:", new Date());
  console.log("User-Agent:", navigator.userAgent);
  console.error("Caught exceptions will be highlighted");
} //Errorhandling


function handleError(err) {
  console.error(err.name, "Caught");
  console.error("Error Message:", err.message);
  console.error("Stack Trace");
  console.error(err['stack']);
} //Create Basic Filter (createFilterHelperModal)


function addtoAvailableFilters() {
  let input = document.getElementById('inputOnPage').value;
  let cB = input.indexOf('AvailableFilters');

  if (cB === -1) {
    handleError(new Error('AvailableFilters not found, could not add filter'));
    console.error('Please ensure the input has near the top: AvailableFilters = {}');
  }

  cB = input.indexOf("{", cB);
  cB = matchClosingBracket(input, cB) - 1;

  while (input.charAt(cB) === ' ' || input.charAt(cB) === '\n') cB--;

  cB += 1;
  let newInput = input.substring(0, cB);
  if (input.charAt(cB - 1) !== '{') newInput += ',';
  newInput += `\n  ${createFilterHelper_filter}`;
  if (input.charAt(cB - 1) === '{') newInput += '\n';
  newInput += input.substring(cB);
  document.getElementById('inputOnPage').value = newInput; //console.log('Critical TODO: 1')
}

let createFilterHelper_sheet = undefined;
let createFilterHelper_column = undefined;
let createFilterHelper_searchTerm = undefined;
let createFilterHelper_name = undefined;
let createFilterHelper_filter = undefined;

function createFilterHelper(stage = 0) {
  let mB = document.getElementById('createFilterHelperModal_body');
  let nMB = "ERROR?";

  function getButton(attributes, value) {
    let btnType = "btn-outline-success";
    if (value.indexOf('Custom') === 0) btnType = "btn-outline-dark";
    return `<button type="button" data-dismiss="modal" data-target="#createFilterHelperModal" class="btn ${btnType} buttonpadding" ${attributes}'>${value}</button>`;
  }

  if (stage === 0) {
    let nMBf = document.getElementById('createFilterHelperModal_footer');
    nMBf.innerHTML = '<button type="button" data-dismiss="modal" data-target="#createFilterHelperModal" class="btn btn-danger">Cancel</button>';
    nMB = "<h5>Please Select an Available Sheet</h5>";

    for (let sheetName in eD.iterableKeys) {
      nMB += getButton(`onclick='createFilterHelper_sheet="${sheetName}"; createFilterHelper(stage = 1)'`, sheetName);
    }
  } else if (stage === 1) {
    //console.log(eD.iterableKeys[createFilterHelper_sheet])
    nMB = "<h5>Please Select an Available Column (Datapoint)</h5>";

    for (let colName in eD.iterableKeys[createFilterHelper_sheet]) {
      nMB += getButton(`onclick='createFilterHelper_column="${colName}"; createFilterHelper(stage = 2)'`, colName); //console.log(colName);
    }
  } else if (stage === 2) {
    nMB = "<h5>Enter your search term (dataterm)</h5>";
    nMB += '<div class="input-group mb-3">';
    nMB += '<input type="text" class="form-control" placeholder="type your term here" id="createFilterHelper_inputTerm"><div class="input-group-append">';
    nMB += `<button onclick='createFilterHelper_searchTerm= document.getElementById("createFilterHelper_inputTerm").value;createFilterHelper(stage = 3)' class="btn btn-success" type="submit">Submit</button>`;
    nMB += '</div>';
    nMB += '</div>';
    nMB += "<h5>...or select from these found dataterms</h5>";
    let dT = eD.getDataTerms(createFilterHelper_sheet, createFilterHelper_column);

    for (property in dT) {
      nMB += getButton(`onclick='createFilterHelper_searchTerm="${property}"; createFilterHelper(stage = 3)'`, `${property} (${dT[property]})`); //console.log(colName);
    }
  } else if (stage === 3) {
    nMB = "<h5>Almost Done: Give your filter a name</h5>";
    nMB += '<div class="input-group mb-3">';
    nMB += '<input type="text" class="form-control" placeholder="type a name for your filter here" id="createFilterHelper_inputTerm"><div class="input-group-append">';
    nMB += `<button onclick='createFilterHelper_name= document.getElementById("createFilterHelper_inputTerm").value;createFilterHelper(stage = 4)' class="btn btn-success" type="submit">View Filter</button>`;
    nMB += '</div>';
    nMB += '</div>';
  } else if (stage === 4) {
    createFilterHelper_filter = `"${createFilterHelper_name}": [ "${createFilterHelper_sheet}", "${createFilterHelper_column}", "${createFilterHelper_searchTerm}"]`;
    nMB = "<h5>Your Filter</h5>";
    nMB += `<p class="text-center bg-dark text-light rounded" style="font-family: 'Courier New'">${createFilterHelper_filter}</p>`;
    nMB += `<button onclick='navigator.clipboard.writeText(createFilterHelper_filter)' class="btn btn-primary text-right">Copy to Clipboard</button>`;
    let nF = "";
    nF += `<button type="button" data-dismiss="modal" data-target="#createFilterHelperModal" class="btn btn-danger">Close (Don\'t Add Filter)</button>`;
    nF += `<button type="button" data-dismiss="modal" data-target="#createFilterHelperModal" class="btn btn-success" onclick='addtoAvailableFilters()'>Add Filter and Close</button>`;
    document.getElementById('createFilterHelperModal_footer').innerHTML = nF;
  }

  mB.innerHTML = nMB;
  $('#createFilterHelperModal').modal('show');
} //Create Command


let createCommandHelper_dict = {
  "plotPieChart": {
    description: "Display a pie-chart with a count of unique IDs under each term in a column.",
    steps: ["createCommandHelper_sheet()", "createCommandHelper_column()", "createCommandHelper_finalize()"]
  },
  "plotDoughnotChart": {
    description: "Display a doughnot-chart with a count of unique IDs under each term in a column.",
    steps: ["createCommandHelper_sheet()", "createCommandHelper_column()", "createCommandHelper_finalize()"]
  },
  "plotBarChart": {
    description: "Display a bar-chart with a count of unique IDs under each term in a column.",
    steps: ["createCommandHelper_sheet()", "createCommandHelper_column()", "createCommandHelper_finalize()"]
  },
  "getN": {
    description: "Get the number of unique IDs ",
    steps: ["createCommandHelper_finalize()"]
  },
  "getIDs": {
    description: "Get a list of all unique IDs",
    steps: ["createCommandHelper_finalize()"]
  },
  "getDataTerms": {
    description: "Get a count of unique IDs under each term in a column.",
    steps: ["createCommandHelper_sheet()", "createCommandHelper_column()", "createCommandHelper_finalize()"]
  },
  "getOneVarStats": {
    description: "Get one variable statistics (mean, median, stddev, IQR, etc.) for a column.",
    steps: ["createCommandHelper_sheet()", "createCommandHelper_column()", "createCommandHelper_finalize()"]
  },
  "getMatches": {
    description: "Get a list of all unique IDs that match a term in a column.",
    steps: ["createCommandHelper_sheet()", "createCommandHelper_column()", "createCommandHelper_term()", "createCommandHelper_finalize()"]
  },
  "getNumMatches": {
    description: "Get the total number of unique IDs that match a term in a column.",
    steps: ["createCommandHelper_sheet()", "createCommandHelper_column()", "createCommandHelper_term()", "createCommandHelper_finalize()"]
  },
  "getDrugStats": {
    description: "Get drug statistics for a specific drug. Includes max dosages, first reaction outcomes, distribution of days between first reaction and max dosage, etc.",
    steps: ["createCommandHelper_column({sheetName: 'Drugs', title: 'Select drug categorization (you probably want active ingredient)', defaultButton: 'btn-outline-danger', preferredValues: ['WHODrug active ingredient', 'WHODrug trade name', 'Reported medication']})", "createCommandHelper_term({title: 'Enter the dataterm to generate drug stats for', sheetName: 'Drugs'})", "createCommandHelper_content({buttons: {'Include Days List':'If selected, the output will contain a list of the days from max dose to reaction. This list could be long. It will contain a descriptive statistics summary.', 'Exclude Days List':'If selected, the output will not contain a list of the days from max dose to reaction. It will contain a descriptive statistics summary.'}})", "createCommandHelper_finalize()"]
  },
  "getOverlapsWithinDatapoint": {
    description: "Outputs a keyed-matrix with counts of the unique ID overlaps for all term combinations in a column. (Warning: could be slow for columns with a large number of unique terms)",
    steps: ["createCommandHelper_sheet()", "createCommandHelper_column()", "createCommandHelper_finalize()"]
  },
  "getDataPoints": {
    description: "Outputs all available sheets & columns, and whether or not the column contains multiple dataterms belonging to the same ID (this occurs when a sheet may have multiple entries per ID)",
    steps: ["createCommandHelper_sheet()", "createCommandHelper_column()", "createCommandHelper_finalize()"]
  }
}; //Future TODO: merge the core of these 3 functions

function createCommandHelper_sheet() {
  let ret = "<h5>Please Select an Available Sheet</h5>";
  let next_stage = createCommandHelper_history.length + 2;

  function getButton(value, btnType = undefined) {
    if (value.indexOf('Custom') === 0) btnType = "btn-outline-dark";
    if (!btnType) btnType = "btn-outline-success";
    return `<button type="button" data-dismiss="modal" data-target="#createCommandHelperModal" class="btn ${btnType} buttonpadding" onclick='createCommandHelper_history.push("${value}");createCommandHelper(stage = ${next_stage})'>${value}</button>`;
  }

  for (let sheetName in eD.iterableKeys) {
    ret += getButton(sheetName);
  }

  return ret;
}

;
let createCommandHelper_filter = undefined;
let createCommandHelper_history = undefined;
let createCommandHelper_command = undefined;

function createCommandHelper_column(args = {}) {
  let ret = "<h5>Please Select an Available Column</h5>";
  if (args.title) ret = `<h5>${args.title}</h5>`;
  let next_stage = createCommandHelper_history.length + 2;
  if (args.sheetName) sheetName = args.sheetName;else sheetName = createCommandHelper_history[next_stage - 3];
  let defaultButton = "btn-outline-success";
  if (args.defaultButton) defaultButton = args.defaultButton;
  let preferredValues = [];

  if (args.preferredValues) {
    for (let elem of args.preferredValues) preferredValues.push(elem);
  } //console.log(args, preferredValues);
  //console.log(args, ret, sheetName, defaultButton, preferredValues);


  function getButton(value, btnType = undefined) {
    if (value.indexOf('Custom') === 0) btnType = "btn-outline-dark";
    if (!btnType) btnType = defaultButton;
    if (preferredValues.includes(value)) return `<button type="button" data-dismiss="modal" data-target="#createCommandHelperModal" class="btn btn-outline-success buttonpadding" onclick='createCommandHelper_history.push("${value}");createCommandHelper(stage = ${next_stage})'>${value}</button>`;
    return `<button type="button" data-dismiss="modal" data-target="#createCommandHelperModal" class="btn ${btnType} buttonpadding" onclick='createCommandHelper_history.push("${value}");createCommandHelper(stage = ${next_stage})'>${value}</button>`;
  }

  for (let colName in eD.iterableKeys[sheetName]) {
    ret += getButton(colName);
  } //console.log(ret);


  return ret;
}

;

function createCommandHelper_term(args = {}) {
  let ret = "<h5>Enter your dataterm (search term)</h5>";
  if (args.title) ret = `<h5>${args.title}</h5>`;
  let next_stage = createCommandHelper_history.length + 2;
  let sheetName;
  if (args.sheetName) sheetName = args.sheetName;else sheetName = createCommandHelper_history[next_stage - 4];
  let colName = createCommandHelper_history[next_stage - 3];
  ret += '<div class="input-group mb-3">';
  ret += '<input type="text" class="form-control" placeholder="type your term here" id="createCommandHelper_inputTerm"><div class="input-group-append">';
  ret += `<button onclick='createCommandHelper_history.push(document.getElementById("createCommandHelper_inputTerm").value); createCommandHelper(stage = ${next_stage})' class="btn btn-success" type="submit">Submit</button>`;
  ret += '</div>';
  ret += '</div>';

  function getButton(term, count, btnType = undefined) {
    if (!btnType) btnType = "btn-outline-success";
    return `<button type="button" data-dismiss="modal" data-target="#createCommandHelperModal" class="btn ${btnType} buttonpadding" onclick='createCommandHelper_history.push("${term}");createCommandHelper(stage = ${next_stage})'>${term} (${count})</button>`;
  }

  ret += "<h5>...or select from these found dataterms</h5>";
  let dT = eD.getDataTerms(sheetName, colName);

  for (let property in dT) {
    ret += getButton(property, dT[property]);
  }

  return ret;
}

function createCommandHelper_content(args = {}) {
  let ret = "<h5>Please Select an option for this command's output</h5>";
  if (args.title) ret = `<h5>${args.title}</h5>`;
  let next_stage = createCommandHelper_history.length + 2;
  let defaultButton = "btn-outline-success";
  if (args.defaultButton) defaultButton = args.defaultButton;
  let preferredValues = [];

  if (args.preferredValues) {
    for (let elem of args.preferredValues) preferredValues.push(elem);
  }

  function getButton(value, btnType = undefined) {
    if (!btnType) btnType = defaultButton;
    if (preferredValues.includes(value)) return `<button type="button" data-dismiss="modal" data-target="#createCommandHelperModal" class="btn btn-outline-success buttonpadding" onclick='createCommandHelper_history.push("${value}");createCommandHelper(stage = ${next_stage})'>${value}</button>`;
    return `<button type="button" data-dismiss="modal" data-target="#createCommandHelperModal" class="btn ${btnType} buttonpadding" onclick='createCommandHelper_history.push("${value}");createCommandHelper(stage = ${next_stage})'>${value}</button>`;
  }

  function getOption(optionName) {
    let nhm = '<hr><div class="row">';
    nhm += `<div class="col-auto align-middle">${getButton(optionName)}</div>`;
    nhm += `<div class="col">${args.buttons[optionName]}</div>`;
    nhm += '</div>';
    return nhm;
  }

  for (let property in args.buttons) ret += getOption(property);

  return ret;
}

function createCommandHelper_finalize() {
  createCommandHelper_command = "";
  if (createCommandHelper_filter) createCommandHelper_command += createCommandHelper_filter + ": ";
  createCommandHelper_command += createCommandHelper_history[0];

  for (let i = 1; i < createCommandHelper_history.length; i++) {
    createCommandHelper_command += ' "';
    createCommandHelper_command += createCommandHelper_history[i];
    createCommandHelper_command += '"';
  } //console.log(createCommandHelper_command);


  let nMB = "<h5>Your Command</h5>";
  nMB += `<p class="text-center bg-dark text-light rounded" style="font-family: 'Courier New'">${createCommandHelper_command}</p>`;
  nMB += `<button onclick='navigator.clipboard.writeText(createCommandHelper_command)' class="btn btn-primary text-right">Copy to Clipboard</button>`;
  let nF = "";
  nF += `<button type="button" data-dismiss="modal" data-target="#createCommandHelperModal" class="btn btn-danger">Close (Don\'t Add Command)</button>`;
  nF += `<button type="button" data-dismiss="modal" data-target="#createCommandHelperModal" class="btn btn-success" onclick='addtoAvailableCommands()'>Add Command and Close</button>`;
  document.getElementById('createCommandHelperModal_footer').innerHTML = nF;
  return nMB;
}

function addtoAvailableCommands() {
  document.getElementById('inputOnPage').value += '\n' + createCommandHelper_command;
}

function createCommandHelper(stage = 0) {
  //console.log(stage, createCommandHelper_history);
  let cMB = document.getElementById('createCommandHelperModal_body');
  let nCMB = "";

  function getCommand(commandName) {
    let nhm = '<div class="row">';
    let button = getButton(`onclick='createCommandHelper_history.push("${commandName}");createCommandHelper(stage = 2)'`, commandName);
    nhm += `<div class="col align-middle" style="padding:0.5em">${createCommandHelper_dict[commandName].description}</div>`;
    nhm += `<div class="col-auto">${button}</div>`;
    nhm += '</div><hr>';
    return nhm;
  }

  function getButton(attributes, value, btnType = undefined) {
    if (!btnType) btnType = "btn-outline-success";
    if (value.indexOf('Custom') === 0) btnType = "btn-outline-dark";
    return `<button type="button" data-dismiss="modal" data-target="#createCommandHelperModal" class="btn ${btnType} buttonpadding" ${attributes}'>${value}</button>`;
  }

  if (stage === 0) {
    let lMBf = document.getElementById('createCommandHelperModal_footer');
    lMBf.innerHTML = '<button type="button" data-dismiss="modal" data-target="#createCommandHelperModal" class="btn btn-danger">Cancel</button>';
    createCommandHelper_filter = undefined;
    nCMB += "<h5>Will this command run against the entire dataset?</h5>";
    nCMB += getButton('onclick="createCommandHelper(stage = 1)"', "Use all the data", "btn-outline-primary");
    nCMB += "<br/><br/> <h5>...or do you want to use a filter for this command?</h5>";
    let filters = getFilters(removeComments(document.getElementById('inputOnPage').value.split('\n')));

    for (let property in filters) {
      nCMB += getButton(`onclick='createCommandHelper_filter="${property}";createCommandHelper(stage = 1)'`, property);
    }
  } else if (stage === 1) {
    createCommandHelper_history = [];
    nCMB = createCommandHelper_filter;
    nCMB = "<h5>Select a command</h5>";
    nCMB += '<div class="container-fluid">';

    for (let property in createCommandHelper_dict) {
      nCMB += getCommand(property);
    }

    nCMB = nCMB.slice(0, -4);
    nCMB += '</div>';
  } else if (stage >= 2) {
    nCMB = eval(createCommandHelper_dict[createCommandHelper_history[0]].steps[stage - 2]);
  }

  cMB.innerHTML = nCMB; //Goddamn is this useless but it looks so cool when its enabled
  //console.log(nCMB);

  $('#createCommandHelperModal').modal('show');
}

document.addEventListener("DOMContentLoaded", function () {
  if (!console) {
    console = {};
  } //Progress Log


  var logger = document.getElementById('consoleOnPage'); //Redirect console output --> Progress Log

  console.log = function () {
    let counter = 0;
    output = "";

    for (let message of Object.values(arguments)) {
      if (counter > 0) output += ' ';

      if (typeof message === 'object') {
        output += JSON && JSON.stringify ? JSON.stringify(message) : String(message);
      } else if (typeof message === 'string') {
        if (message.indexOf('\n') >= 0) {
          for (let pmessage of message.split('\n')) {
            output += pmessage;
            output += '<br />';
          }
        } else output += message;
      } else {
        output += message;
      }

      counter += 1;
    }

    logger.innerHTML += output;
    logger.innerHTML += '<br />';
  }; //Redirect captured errors --> Progress Log


  console.error = function () {
    let counter = 0;
    errorOutput = "<span class='bg-danger'>";
    let toast = true;

    for (let message of Object.values(arguments)) {
      if (counter > 0) errorOutput += ' ';

      if (typeof message === 'object') {
        errorOutput += JSON && JSON.stringify ? JSON.stringify(message) : String(message);
      } else {
        errorOutput += message;
        if (message === 'Caught exceptions will be highlighted') toast = false;
      }

      counter += 1;
    }

    errorOutput += `</span>`;
    logger.innerHTML += errorOutput;
    logger.innerHTML += '<br />';

    if (toast) {
      $('#notifyError').toast({
        delay: 5000
      });
      $('#notifyError').toast('show');
    }
  }; //Initialize Progress Log


  initPageConsole(); //Initialize File Loading Management

  new LoadFile({
    inputElement: document.getElementById('inputfile'),
    dropElement: document.getElementById('dropfile')
  }); //

  initInputTemplate();
  clearTextArea('outputOnPage');
});
'use strict'; //const ComplexSet = require('./ComplexSet.js');
//Map, key: any standard key, value: a ComplexSet
//if a key isn't in the set, the value of that key should correspond to the complement of the empty set

class MappedComplexSet extends Map {
  static getCopy(MappedComplexSetA) {
    return MappedComplexSetA.getCopy();
  }

  static union(MappedComplexSetA, MappedComplexSetB) {
    let ret = MappedComplexSetA.getCopy();
    ret.unionUpdate(MappedComplexSetB);
    return ret;
  }

  static intersection(MappedComplexSetA, MappedComplexSetB) {
    let ret = MappedComplexSetA.getCopy();
    ret.intersectionUpdate(MappedComplexSetB);
    return ret;
  }

  static difference(MappedComplexSetA, MappedComplexSetB) {
    let ret = MappedComplexSetA.getCopy();
    ret.differenceUpdate(MappedComplexSetB);
    return ret;
  }

  static symmetricDifference(MappedComplexSetA, MappedComplexSetB) {
    let ret = MappedComplexSetA.getCopy();
    ret.symmetricDifferenceUpdate(MappedComplexSetB);
    return ret;
  }

  getCopy() {
    let copy = new MappedComplexSet();

    for (let [key, ComplexSetB] of this.entries()) {
      copy.set(key, new ComplexSet(ComplexSetB));
    }

    return copy;
  }

  unionUpdate(MappedComplexSetB) {
    for (let [key, ComplexSetB] of MappedComplexSetB.entries()) {
      //console.log(key, this.get(key), ComplexSetB)
      if (this.has(key)) this.get(key).unionUpdate(ComplexSetB);
    }
  }

  intersectionUpdate(MappedComplexSetB) {
    for (let key of this.keys()) {
      //console.log(key, '1', this.get(key).toStr())
      if (!MappedComplexSetB.has(key)) this.delete(key);else {
        //console.log(key, '2', MappedComplexSetB.get(key).toStr())
        this.get(key).intersectionUpdate(MappedComplexSetB.get(key));
      } //console.log(key, '3', this.get(key).toStr())
      //console.log();
    }

    for (let [key, ComplexSetB] of MappedComplexSetB.entries()) {
      if (!this.has(key)) this.set(key, ComplexSetB.getCopy());
    }
    /*
    for(let [key, ComplexSetB] of MappedComplexSetB.entries()){
    	if(!this.has(key)) this.delete(key);
    	else this.get(key).intersectionUpdate(ComplexSetB);
    }
    */

  }

  differenceUpdate(MappedComplexSetB) {
    for (let [key, ComplexSetB] of MappedComplexSetB.entries()) {
      if (this.has(key)) {
        this.get(key).differenceUpdate(ComplexSetB);
      } else this.set(key, ComplexSet.complement(ComplexSetB));
    }
  }

  symmetricDifferenceUpdate(MappedComplexSetB) {
    for (let [key, ComplexSetB] of MappedComplexSetB.entries()) {
      if (this.has(key)) {
        this.get(key).symmetricDifferenceUpdate(ComplexSetB);
      } else this.set(key, ComplexSet.complement(ComplexSetB));
    }
  }

  toStr() {
    let ret = `MappedComplexSet(${this.size}) {\n`;

    for (let [key, aComplexSet] of this.entries()) {
      ret += `\t(${key}): ${aComplexSet.toStr()}\n`;
    }

    ret += "}";
    return ret;
  }

} //module.exports = MappedComplexSet

/*Proper Output of testMappedComplexSet()
Union
MappedComplexSet(3) [Map] {
  'A' => ComplexSet(4) [Set] { 1, 2, 3, 4, complementMode: false },
  'B' => ComplexSet(5) [Set] { 2, 3, 4, 5, 6, complementMode: false },
  'C' => ComplexSet(6) [Set] { 3, 4, 5, 6, 1, 2, complementMode: false }
}
Intersection
MappedComplexSet(3) [Map] {
  'A' => ComplexSet(2) [Set] { 2, 3, complementMode: false },
  'B' => ComplexSet(1) [Set] { 3, complementMode: false },
  'C' => ComplexSet(2) [Set] { 3, 4, complementMode: false }
}
Difference
MappedComplexSet(3) [Map] {
  'A' => ComplexSet(2) [Set] { 1, 4, complementMode: false },
  'B' => ComplexSet(1) [Set] { 2, complementMode: false },
  'C' => ComplexSet(2) [Set] { 5, 6, complementMode: false }
}
Symmetric Difference
MappedComplexSet(3) [Map] {
  'A' => ComplexSet(2) [Set] { 1, 4, complementMode: false },
  'B' => ComplexSet(4) [Set] { 2, 4, 5, 6, complementMode: false },
  'C' => ComplexSet(4) [Set] { 5, 6, 1, 2, complementMode: false }
}
*/
//testMappedComplexSet()


function testMappedComplexSet() {
  let myMap1 = new MappedComplexSet();
  let myMap2 = new MappedComplexSet();

  let reInit = function () {
    myMap1 = new MappedComplexSet();
    myMap1.set('A', new ComplexSet([1, 2, 3, 4]));
    myMap1.set('B', new ComplexSet([2, 3]));
    myMap1.set('C', new ComplexSet([3, 4, 5, 6]));
    myMap2 = new MappedComplexSet();
    myMap2.set('A', new ComplexSet([2, 3]));
    myMap2.set('B', new ComplexSet([3, 4, 5, 6]));
    myMap2.set('C', new ComplexSet([1, 2, 3, 4]));
  };

  reInit(); //console.log('MappedComplexSet A')
  //console.log(myMap1)
  //console.log('MappedComplexSet B')
  //console.log(myMap2)

  console.log('Union');
  console.log(MappedComplexSet.union(myMap1, myMap2));
  console.log('Intersection');
  console.log(MappedComplexSet.intersection(myMap1, myMap2));
  console.log('Difference');
  console.log(MappedComplexSet.difference(myMap1, myMap2));
  console.log('Symmetric Difference');
  console.log(MappedComplexSet.symmetricDifference(myMap1, myMap2));
}
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var pM;
var pW;

function testPlots() {
  try {
    pM.clearPlots();
    PlotCreator.plotPieChart('Cases', 'Country of primary source');
    PlotCreator.plotBarChart('Cases', 'Custom|Continent of primary source');
    PlotCreator.plotDoughnotChart('Cases', 'Sex');
    pM.refreshPlots();
  } catch (err) {
    handleError(err);
  } //PlotCreator.plotOneVarR(boxplotData);

}

class PlotManager {
  constructor(divID) {
    _defineProperty(this, "divID", null);

    _defineProperty(this, "allDicts", null);

    _defineProperty(this, "maxPerRow", 2);

    this.divID = divID;
    this.allDicts = [];
  }

  setMaxPerRow(n) {
    this.maxPerRow = n;
    this.refreshPlots();
  }

  addPlotNoUpdate(dict) {
    this.allDicts.push(dict);
    console.log("[INFO]", `Loaded Plot #${this.allDicts.length} `);
  }

  addPlot(dict) {
    this.addPlotNoUpdate(dict);
    this.refreshPlots(); //this.plots.innerHTML += `<div class="container" id="Plot"><canvas id="${cID}"></canvas></div>`
    //for(let chart of this.charts) chart.update();
    //this.charts.push();
  }

  refreshPlots() {
    //console.log(this.divID)
    let plotsDiv = document.getElementById(this.divID);
    plotsDiv.innerHTML = ""; //create div layout

    let currPlot = 0;
    let currRow = '';

    for (let i = 0; i < this.allDicts.length; i++) {
      if (currPlot % this.maxPerRow === 0) {
        currRow = '';
      }

      let cID = `${this.divID}${i}`;
      currRow += `<div class="col container" id="Plot"><canvas id="${cID}"></canvas></div>`;

      if (currPlot % this.maxPerRow === this.maxPerRow - 1) {
        plotsDiv.innerHTML += '<div class="row">' + currRow + '</div>';
      }

      currPlot += 1;
    }

    if (currRow !== '' && currPlot % this.maxPerRow !== 0) plotsDiv.innerHTML += '<div class="row">' + currRow + '</div>'; //can't merge for loops

    for (let i = 0; i < this.allDicts.length; i++) {
      let cID = `${this.divID}${i}`;
      let ctx = document.getElementById(cID).getContext('2d');
      new Chart(ctx, this.allDicts[i]);
    }
  }

  clearPlots() {
    console.log("[INFO]", 'Removed All Plots');
    this.allDicts = [];
    document.getElementById(this.divID).innerHTML = "";
  }

}

class ColorGenerator {
  constructor() {
    _defineProperty(this, "bgColors", ['rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)', 'rgba(255, 206, 86, 0.2)', 'rgba(75, 192, 192, 0.2)', 'rgba(153, 102, 255, 0.2)', 'rgba(255, 159, 64, 0.2)']);

    _defineProperty(this, "colorIdx", void 0);

    this.colorIdx = -1;
  }

  getNextColor() {
    this.colorIdx += 1;
    let n = this.bgColors.length;
    let bgColor = this.bgColors[this.colorIdx % n];
    let bdColor = bgColor.split(' ');
    bdColor[bdColor.length - 1] = '1)';
    bdColor = bdColor.join(' ');
    return [bgColor, bdColor];
  }

}

class PlotCreator {
  //all datasets must have the label and data properties
  static addCountChart(sheetName, colName, type, filterName = null, refresh = true) {
    let rawData = eD.getDataTerms(sheetName, colName);
    let dict = {
      type: type,
      data: {
        labels: [],
        datasets: [{
          label: "# of Unique IDs",
          borderWidth: 1,
          hoverBorderWith: 2,
          data: [],
          backgroundColor: [],
          borderColor: []
        }]
      },
      options: {
        legend: {
          position: 'top'
        },
        title: {
          display: true,
          position: 'top',
          text: `${sheetName} by ${colName}`
        }
      }
    };
    if (type === 'pie' || type == 'doughnut') dict.options.legend.position = 'bottom';
    if (filterName) dict.options.title.text += `, under filter ${filterName}`;
    let colors = new ColorGenerator();

    for (let [dataPoint, count] of Object.entries(rawData)) {
      //console.log(dataPoint, count);
      if (dataPoint === '') dataPoint = 'Unknown';else if (!dataPoint) dataPoint = String(dataPoint);
      let [bgColor, bdColor] = colors.getNextColor();
      dict.data.datasets[0].data.push(count);
      dict.data.datasets[0].backgroundColor.push(bgColor);
      dict.data.datasets[0].borderColor.push(bdColor);
      dict.data.labels.push(dataPoint);
    }

    if (dict.data.labels.length > 25) {
      dict.options.title.text += `, labels hidden (${dict.data.labels.length} labels)`;
      dict.options.legend.display = false;
    } //console.log(dict)


    if (refresh) pM.addPlot(dict);else pM.addPlotNoUpdate(dict);
  }

  static plotPieChart(sheetName, colName, filterName = null, refresh = false) {
    PlotCreator.addCountChart(sheetName, colName, 'pie', filterName = filterName, refresh = refresh);
  }

  static plotDoughnotChart(sheetName, colName, filterName = null, refresh = false) {
    PlotCreator.addCountChart(sheetName, colName, 'doughnut', filterName = filterName, refresh = refresh);
  }

  static plotBarChart(sheetName, colName, filterName = null, refresh = false) {
    PlotCreator.addCountChart(sheetName, colName, 'bar', filterName = filterName, refresh = refresh);
  }

}

document.addEventListener("DOMContentLoaded", function () {
  pM = new PlotManager('Plots');
});
'use strict'; //All Sample-based Statistics, not population
//linearly interpolated
//exclusive, expected value of rank order statistics

class Statistics {
  static quantiles(sorted, intervals) {
    let len = sorted.length;
    if (len < 2) return null;
    let ret = [];

    for (let p = 1; p < intervals; p += 1) {
      let delt = p * (len + 1) / intervals;
      let idx = Math.floor(delt);
      delt -= idx;
      if (idx < 1) idx = 1;else if (idx >= len) idx = len - 1;
      ret.push(sorted[idx - 1] * (1 - delt) + sorted[idx] * delt); //console.log(len, '\t', p, '\t', idx, '\t', delt, '\t', ret[ret.length-1])
    }

    return ret;
  }

  static meanStdDev(data) {
    let mean = data.reduce((a, b) => a + b) / data.length;
    let variance = 0;
    data.forEach(elem => variance += Math.pow(elem - mean, 2));
    variance = variance / data.length; //console.log(mean, variance);

    return [mean, Math.pow(variance, 1 / 2)];
  }

  static oneVarStats(data) {
    let ret = {
      n: data.length,
      quartile1: undefined,
      quartile3: undefined,
      median: undefined,
      mean: undefined,
      stddev: undefined,
      //sample standard deviation
      min: undefined,
      max: undefined,
      range: undefined
    };
    if (ret['n'] == 0) return ret;
    let sorted = data.sort((a, b) => a - b);
    ret['min'] = sorted[0];
    ret['max'] = sorted[sorted.length - 1];
    ret['range'] = ret['max'] - ret['min'];

    if (ret['n'] == 1) {
      ret['mean'] = sorted[0];
      ret['median'] = sorted[0];
      ret['stddev'] = 0;
      return ret;
    }

    [ret['quartile1'], ret['median'], ret['quartile3']] = Statistics.quantiles(sorted, 4); //console.log(sorted);

    [ret['mean'], ret['stddev']] = Statistics.meanStdDev(sorted);
    return ret;
  }

} //module.exports = {oneVarStats}
//console.log('--',quantiles([1,2,3,4,5,6], 4))
//console.log(oneVarStats([1,2,3,4,5,6]))
//''.sad()

